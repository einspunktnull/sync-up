export const EXIT_CODE_UNHANDLED: number = 12345;
type AsyncRun = (...args: any[]) => Promise<void>;
export const run = (asyncRun: AsyncRun) => {

    asyncRun()
        .catch((err: any) => {
            console.error(`Unhandled Error:`, err, process.argv);
            process.exit(EXIT_CODE_UNHANDLED);
        });
};
