export type GetSetConfigDto<T> = { path?: string, value?: T };
