import {DeviceConfig, SyncUpConfig} from '../../core/config/SyncUpConfig';
import {ConfigLoader} from '@beautils/all';
import {isPlainObject, isString} from 'lodash';

export class Helper {

    public static async load(configFile: string): Promise<SyncUpConfig> {
        return ConfigLoader.load<SyncUpConfig>({configYaml: configFile});
    }

    public static validate(config: SyncUpConfig): void {
        const deviceConfigs: DeviceConfig[] = config.devices;
        const deviceIds: string[] = [];
        for (const deviceConfig of deviceConfigs) {
            const currId: string = deviceConfig.id;
            const hasId: boolean = deviceIds.find(id => id === currId) !== undefined;
            if (hasId) throw new Error('duplicate device id: ' + currId);
            deviceIds.push(currId);
        }
    }

    public static isConfig(config: SyncUpConfig | string): config is SyncUpConfig {
        return isPlainObject(config);
    }

    public static isConfigFile(config: SyncUpConfig | string): config is string {
        return isString(config);
    }
}
