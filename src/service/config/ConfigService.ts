import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {SyncUpConfig} from '../../core/config/SyncUpConfig';
import lodash from 'lodash';
import {IpcService} from '../ipc/IpcService';
import {IpcEvent} from '../../core/constants';
import {ProcessInfoService} from '../processInfo/ProcessInfoService';
import {Initializable} from '../../lib/Initializable';
import {Properties} from '../../lib/properties/Properties';
import {GetSetConfigDto} from './types';
import {Helper} from './Helper';
import {DevUtil} from '@beautils/all';
import {Property} from '../../lib/properties/types';

@Singleton
@OnlyInstantiableByContainer
export class ConfigService extends Initializable<void> {

    private isMother: boolean;
    private _configg: Properties<SyncUpConfig>;
    private _config: SyncUpConfig;

    @Inject
    protected readonly processInfo: ProcessInfoService;

    @Inject
    private readonly ipcService: IpcService;

    public async get<T>(path: string = 'root'): Promise<T> {
        this.expectInitialized;
        if (this.isMother) {
            const configRoot: Property<SyncUpConfig> = this._configg.root;
            const property: Property<any> = path === 'root' ? configRoot : lodash.get(configRoot, path);
            return property.get();
        }
        return this.askGetConfig(path);
    }

    public async set<T>(path: string, value: T): Promise<void> {
        this.expectInitialized;
        if (this.isMother) throw DevUtil.notImplementedError;
        return this.askSetConfig(path, value);
    }

    protected async initialize(configOrConfigFile?: SyncUpConfig | string): Promise<void> {
        this.isMother = configOrConfigFile !== undefined;
        if (!this.isMother) return;
        let config: SyncUpConfig;
        if (Helper.isConfigFile(configOrConfigFile)) {
            config = await Helper.load(configOrConfigFile);
        } else if (Helper.isConfig(configOrConfigFile)) {
            config = configOrConfigFile;
        }
        Helper.validate(config);
        this._config = config;
        this._configg = new Properties<SyncUpConfig>(config);
        this.ipcService.answer(
            IpcEvent.GET_CONFIG,
            this.answerGetConfig.bind(this)
        );
        this.ipcService.answer(
            IpcEvent.SET_CONFIG,
            this.answerSetConfig.bind(this)
        );
    }

    private async answerGetConfig<T>(sender: string, dto: GetSetConfigDto<T>): Promise<GetSetConfigDto<T>> {
        // console.log(`[${this.processInfo.id}]`, 'ConfigService.answerGetConfig', sender);
        const value: T = await this.get(dto.path);
        return {value: value};
    }

    private async answerSetConfig<T>(sender: string, dto: GetSetConfigDto<T>): Promise<GetSetConfigDto<T>> {
        // console.log(`[${this.processInfo.id}]`, 'ConfigService.answerSetConfig', sender);
        await this.set(dto.path, dto.value);
        return {};
    }

    private async askGetConfig<T>(path: string): Promise<T> {
        // console.log(`[${this.processInfo.id}]`, 'ConfigService.askGetConfig', path);
        const dtoOut: GetSetConfigDto<T> = {path};
        const dtoIn: GetSetConfigDto<T> = await this.ipcService.ask(IpcEvent.GET_CONFIG, dtoOut);
        return dtoIn.value;
    }

    private async askSetConfig<T>(path: string, value: T): Promise<void> {
        // console.log(`[${this.processInfo.id}]`, 'ConfigService.askSetConfig', path, value);
        const dtoOut: GetSetConfigDto<T> = {path, value};
        await this.ipcService.ask(IpcEvent.SET_CONFIG, dtoOut);
    }

}
