import {OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {v1 as uuidV1} from 'uuid';
import {Initializable} from '../../lib/Initializable';

@Singleton
@OnlyInstantiableByContainer
export class ProcessInfoService extends Initializable<void> {

    private _id: string;
    private _motherProcessId: string;
    private _isSingleInstance: boolean;

    public initialize(motherProcessId: string, idPrefixOrId: string, isSingleInstance: boolean): void {
        this._motherProcessId = motherProcessId;
        this._isSingleInstance = isSingleInstance;
        this._id = Helper.createId(idPrefixOrId, isSingleInstance);
        this._isSingleInstance = isSingleInstance;
        this._motherProcessId = Helper.createMotherProcessId(motherProcessId, idPrefixOrId, isSingleInstance);
    }

    public get id(): string {
        return this._id;
    }

    public get motherProcessId(): string {
        return this._motherProcessId;
    }

    public get isSingleInstance(): boolean {
        return this._isSingleInstance;
    }

    public get isMotherProcess(): boolean {
        return this.id === this.motherProcessId;
    }

}

class Helper {
    public static createMotherProcessId(motherProcessId: string, idPrefixOrId: string, isSingleInstance: boolean): string {
        return motherProcessId === idPrefixOrId ?
            Helper.createId(idPrefixOrId, isSingleInstance) :
            motherProcessId;
    }

    public static createId(idPrefixOrId: string, isSingleInstance: boolean): string {
        return isSingleInstance ? `${idPrefixOrId}` : `${idPrefixOrId}-${uuidV1()}`;
    }
}
