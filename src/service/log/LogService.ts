import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {ConfigService} from '../config/ConfigService';
import path from 'path';
import {LogDir, LogLevel} from '../../core/constants';
import pino, {Logger} from 'pino';
import moment from 'moment';
import mkdirp from 'mkdirp';
import {ProcessInfoService} from '../processInfo/ProcessInfoService';
import {Initializable} from '../../lib/Initializable';
import jsonata, {Expression} from 'jsonata';
import {Helper} from './Helper';
import {LogEntry} from './types';
import {ErrorStack, ErrorUtil} from '../../lib/ErrorUtil';
import {DeviceConfig, SyncUpConfig} from '../../core/config/SyncUpConfig';

@Singleton
@OnlyInstantiableByContainer
export class LogService extends Initializable<void> {

    private static readonly REGEX_THIS_NAME: RegExp = new RegExp(LogService.name);

    @Inject
    private readonly processInfo: ProcessInfoService;

    @Inject
    private readonly configService: ConfigService;

    private pinoLogger: Logger;
    private cache: LogEntry[] = [];
    private consoleLogFilter: string[];
    private config: SyncUpConfig;

    public fatal(...args: any[]): void {
        this.logg(null, LogLevel.fatal, args);
    }

    public error(...args: any[]): void {
        this.logg(null, LogLevel.error, args);
    }

    public warn(...args: any[]): void {
        this.logg(null, LogLevel.warn, args);
    }

    public log(...args: any[]): void {
        this.info(...args);
    }

    public info(...args: any[]): void {
        this.logg(null, LogLevel.info, args);
    }

    public debug(...args: any[]): void {
        this.logg(null, LogLevel.debug, args);
    }

    public trace(...args: any[]): void {
        this.logg(null, LogLevel.trace, args);
    }

    public fatalCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.fatal, args);
    }

    public errorCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.error, args);
    }

    public warnCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.warn, args);
    }

    public logCh(channel: any, ...args: any[]): void {
        this.infoCh(channel, ...args);
    }

    public infoCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.info, args);
    }

    public debugCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.debug, args);
    }

    public traceCh(channel: any, ...args: any[]): void {
        this.logg(channel, LogLevel.trace, args);
    }

    protected async initialize(args: any): Promise<void> {
        this.config = await this.configService.get();
        const logDir: string = path.resolve(this.config.app.dataDir + path.sep + LogDir);
        await mkdirp(logDir);
        const logFileLogDataName: string = moment().format('YYYYMMDDHHmmss') + '.log';
        const logFile: string = logDir + path.sep + logFileLogDataName;
        this.pinoLogger = pino({
            level: this.config.logging.logger.level,
            timestamp: true
        }, pino.destination(logFile));
        this.cache.forEach((logEntry: LogEntry) => {
            this.loggToLogger(logEntry);
        });
        if (
            this.config.debugging?.enabled &&
            this.config.debugging?.logging?.exclude !== undefined
        ) {
            this.consoleLogFilter = this.config.debugging.logging.exclude;
        }
    }

    private logg(channel: any, logLevel: LogLevel, args: any[]): void {
        const logEntry: LogEntry = {
            level: logLevel,
            processId: this.processInfo.id,
            channel: channel,
            moment: moment(),
            args: args,
            location: ErrorUtil.createErrorStack().locations
                .find(loc => !LogService.REGEX_THIS_NAME.test(loc.raw))
        };
        this.isInitialized ? this.loggToCache(logEntry) : this.loggToLogger(logEntry);
        this.loggToConsole(logEntry);
    }

    private loggToCache(logEntry: LogEntry): void {
        this.cache.push(logEntry);
    }

    private loggToLogger(logEntry: LogEntry): void {
        this.pinoLogger[logEntry.level]({
            date: Helper.momentToString(logEntry.moment),
            processId: `[${logEntry.processId}]`,
            channel: `[${Helper.channelToString(logEntry.channel)}]`,
            args: logEntry.args,
        });
    }

    private loggToConsole(logEntry: LogEntry): void {

        const logLevelOfEntry: number = pino.levels.values[logEntry.level];
        // console.log('logLevelOfEntry', pino.levels.values, logLevelOfEntry, this.consoleLogLevel);
        const consoleLogLevel: number = pino.levels.values[this.config.logging.console.level];
        if (logLevelOfEntry < consoleLogLevel) return;

        const dateStr: string = Helper.momentToString(logEntry.moment);
        const processId: string = `[${logEntry.processId}]`;
        const channelName: string =
            logEntry.channel ?
                `[${Helper.channelToString(logEntry.channel)}]` :
                `[${logEntry.location.at}]`;

        if (this.consoleLogFilter) {
            const logData = {
                processId: processId,
                channel: channelName,
                args: logEntry.args
            };
            // console.log('>>>>> consoleLogFilter >>>>>', this.consoleLogFilter, logData);
            const jsonataStr: string = this.consoleLogFilter.join(' or ');
            // console.log('jsonataStr',jsonataStr);
            const expression: Expression = jsonata(jsonataStr);
            const result: any = expression.evaluate(logData);
            // console.log('>>>>> result >>>>>', result);
            if (result === true) return;
        }

        const args: any[] = [
            dateStr,
            processId,
            channelName,
            ...logEntry.args,
        ];

        if (
            this.config.debugging?.enabled &&
            this.config.debugging?.logging?.showFileAndLineNum === true
        ) {
            args.splice(2, 0, `[${logEntry.location.fileName}:${logEntry.location.line}]`);
        }

        console.log.apply(console, args);
    }

}



