import {LogLevel} from '../../core/constants';
import {Moment} from 'moment';
import {ErrorLocation} from '../../lib/ErrorUtil';

export interface LogEntry {
    processId: string;
    level: LogLevel,
    moment: Moment,
    channel: any,
    args: any[],
    location: ErrorLocation,
}
