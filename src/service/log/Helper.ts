import lodash from 'lodash';
import util from 'util';
import {Moment} from 'moment';

export class Helper {
    public static channelToString(channel: any): string {
        if (lodash.isString(channel.name)) return channel.name;
        if (channel.constructor?.name === 'ioc_wrapper')
            return channel.__proto__?.constructor?.__parent?.name || 'ioc_wrapper';
        if (lodash.isString(channel)) return channel;
        if (channel?.constructor?.name) return channel?.constructor?.name;
        return util.inspect(channel, {depth: 1});
    }

    public static momentToString(moment: Moment): string {
        return moment.format('YYYY-MM-DD HH:mm:ss');
    }

}
