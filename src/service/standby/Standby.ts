import {Hdparm} from '../../lib/hdparm/Hdparm';
import {Subject} from 'rxjs';
import {DateAndTimeUtil, PromiseUtil} from '@beautils/all';

export class Standby {

    private readonly hdparm: Hdparm;
    private static readonly TIMEOUT: number = 1000;
    private isRunning: boolean = false;
    private timeout: NodeJS.Timeout;
    private resolve: (value: (PromiseLike<void> | void)) => void;
    private readonly device: string;

    public readonly errors: Subject<Error> = new Subject<Error>();
    private interval: number;

    public constructor(device: string) {
        this.device = device;
        this.hdparm = new Hdparm(device);
    }

    public async start(intervalMs:number | string = Standby.TIMEOUT): Promise<void> {
        this.interval = DateAndTimeUtil.toMilliseconds(intervalMs);
        if (this.isRunning) throw new Error('already running');
        this.isRunning = true;
        PromiseUtil.justCatch(this.runLoop());
    }

    public async stop(): Promise<void> {
        clearTimeout(this.timeout);
        this.isRunning = false;
        if (this.resolve) this.resolve();
    }

    public async kill(): Promise<void> {
        return this.stop();
    }

    private async runLoop(): Promise<void> {
        while (this.isRunning) await this.exec();
    }

    private exec(): Promise<void> {
        return new Promise<void>(async (resolve) => {
            this.resolve = resolve;
            try {
                await this.hdparm.standby();
            } catch (e) {
                this.errors.next(e);
            }
            if (!this.isRunning) return;
            this.timeout = setTimeout(() => {
                const resolve = this.resolve;
                this.resolve = undefined;
                resolve();
            }, Standby.TIMEOUT);
        });
    }

}
