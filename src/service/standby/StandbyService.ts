import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {Standby} from './Standby';
import {Killable} from '../../lib/Killable';
import {PromiseUtil} from '@beautils/all';
import {Subscription} from 'rxjs';
import {LogService} from '../log/LogService';

interface StandbyWrap {
    standby: Standby;
    errorSub: Subscription;
}

@Singleton
@OnlyInstantiableByContainer
export class StandbyService implements Killable {

    @Inject
    private readonly logService: LogService;

    private readonly standbies: Map<string, StandbyWrap> = new Map<string, StandbyWrap>();

    public async forceStandby(device: string): Promise<void> {
        const standby: Standby = this.createStandby(device);
        return standby.start();
    }

    public async stop(device?: string): Promise<void> {
        // stop one
        if (device) return this.stopOne(device);
        // or stop all
        await PromiseUtil.allForEach(this.getDevices(), this.stopOne.bind(this));
    }

    public async kill(): Promise<void> {
        await PromiseUtil.allForEach(this.getDevices(), this.killOne.bind(this));
    }

    private async killOne(device: string): Promise<void> {
        return this.killOrStopOne(device, true);
    }

    private async stopOne(device: string): Promise<void> {
        return this.killOrStopOne(device, false);
    }

    private async killOrStopOne(device: string, kill: boolean): Promise<void> {
        const standbyWrap: StandbyWrap = this.standbies.get(device);
        this.standbies.delete(device);
        standbyWrap.errorSub.unsubscribe();
        return kill ? standbyWrap.standby.kill() : standbyWrap.standby.stop();
    }

    private getDevices(): string[] {
        return Array.from(this.standbies.keys());
    }

    private createStandby(device: string): Standby {
        const standbyExists: boolean = this.standbies.get(device) !== undefined;
        if (standbyExists) throw new Error('device is already registered');
        const standby: Standby = new Standby(device);
        const subscription: Subscription = standby.errors.subscribe((standByError: Error) => {
            this.logService.debug(device, standByError);
        });
        this.standbies.set(device, {
            standby: standby,
            errorSub: subscription,
        });
        return standby;
    }

}
