import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {DeviceConfig, JobConfig} from '../../core/config/SyncUpConfig';
import {ConfigService} from '../config/ConfigService';
import {Job} from '../../core/job/Job';
import {DdJob} from '../../job/DdJob';
import {RsyncJob} from '../../job/RsyncJob';
import {ProcessUtil, PromiseUtil} from '@beautils/all';
import {JobState, JobType} from '../../core/constants';

@Singleton
@OnlyInstantiableByContainer
export class JobService {

    @Inject
    private readonly configService: ConfigService;

    private readonly jobs: Job[] = [];

    private get pendingJobs(): Job[] {
        return this.jobs.filter(job => job.state === JobState.PENDING);
    }

    private get runningJobs(): Job[] {
        return this.jobs.filter(job => job.state === JobState.RUNNING);
    }

    private get killingJobs(): Job[] {
        return this.jobs.filter(job => job.state === JobState.KILLING);
    }

    private get killedJobs(): Job[] {
        return this.jobs.filter(job => job.state === JobState.KILLED);
    }

    private get finishedJobs(): Job[] {
        return this.jobs.filter(job => job.state === JobState.FINISHED);
    }

    public async execute(deviceConfigs: DeviceConfig[]): Promise<void> {
        await ProcessUtil.expectSuperuser();
        deviceConfigs.forEach((deviceConfig: DeviceConfig) => {
            // console.log('deviceConfig', deviceConfig);
            deviceConfig.jobs?.forEach((jobConfig: JobConfig) => {
                this.createJob(deviceConfig.id, jobConfig);
            });
        });
        for (const job of this.jobs) await job.run();
    }

    public async kill(deviceConfigs: DeviceConfig[]): Promise<void> {
        await PromiseUtil.allForEach(this.jobs, job => job.stop());
    }

    private createJob(deviceId: string, jobConfig: JobConfig): void {
        // console.log('createJob', deviceId, jobConfig);
        let job: Job;
        switch (jobConfig.type) {
            case JobType.dd:
                job = new DdJob(deviceId, jobConfig);
                break;
            case JobType.rsync:
                job = new RsyncJob(deviceId, jobConfig);
                break;
            default:
                throw new Error('Unknown Job Type: [' + jobConfig.type + ']');
        }
        this.jobs.push(job);
    }
}

