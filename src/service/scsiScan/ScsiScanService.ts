import {OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {ProcessUtil, PromiseUtil} from '@beautils/all';

interface SleepData {
    timeout: NodeJS.Timeout;
    resolve: () => void;
}

@Singleton
@OnlyInstantiableByContainer
export class ScsiScanService {

    /**
     * Got it form here:
     * https://forum.manjaro.org/t/solved-how-do-i-enable-sata-hotplug/2911/6
     */
    private static readonly Command: string = 'echo 0 0 0 | tee /sys/class/scsi_host/host*/scan';

    private isRunning: boolean = false;
    private isAlive: boolean = false;
    private runningScanPromise: Promise<void>;
    private readonly sleepData: SleepData = {
        timeout: null,
        resolve: null
    };

    public startScan(intervalMs: number): void ;
    public startScan(): void ;
    public startScan(intervalMs: number = -1): void {
        this.scan(intervalMs).catch(() => {});
    }

    public async scan(intervalMs: number): Promise<void>;
    public async scan(): Promise<void>;
    public async scan(intervalMs: number = -1): Promise<void> {
        if (this.isRunning) throw new Error('already scanning');
        this.isRunning = true;
        this.isAlive = true;
        if (intervalMs < 0) this.runningScanPromise = this.scanOnce();
        else this.runningScanPromise = this.scanPermanently(intervalMs);
        await this.runningScanPromise;
        this.reset();
    }

    public async kill(): Promise<void> {
        if (!this.isRunning) throw new Error('not scanning');
        this.isRunning = false;
        this.isAlive = false;
        if (this.sleepData.resolve) this.sleepData.resolve();
        clearTimeout(this.sleepData.timeout);
        await this.runningScanPromise;
        this.reset();
    }

    private async scanOnce(): Promise<void> {
        await ProcessUtil.exec(ScsiScanService.Command);
    }

    private async scanPermanently(intervalMs: number): Promise<void> {
        while (this.isAlive) {
            await this.scanOnce();
            if (!this.isAlive) return;
            await this.sleep(intervalMs);
        }
    }

    private sleep(durationMs: number): Promise<void> {
        return new Promise<void>((resolve) => {
            this.sleepData.resolve = resolve;
            this.sleepData.timeout = setTimeout(this.sleepData.resolve, durationMs);
        });
    }

    private reset(): void {
        this.isRunning = false;
        this.isAlive = false;
        this.sleepData.timeout = null;
        this.sleepData.resolve = null;
        this.runningScanPromise = undefined;
    }

}
