import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {SystemInfoService} from '../systemInfo/SystemInfoService';
import {ScsiScanService} from '../scsiScan/ScsiScanService';
import {CpuInfo, DiskInfo, DiskPartitionInfo, GraphicsAdapterInfo, MainboardInfo} from '../systemInfo/types';
import {ConfigService} from '../config/ConfigService';
import {DeviceConfig, FindDiskConfig, FindPartitionConfig, HardwareConfig} from '../../core/config/SyncUpConfig';
import {ArrayAsyncUtil, AsyncLoopUtil, BytesUtil, ProcessUtil} from '@beautils/all';
import fsPromises from 'fs/promises';
import {Stats} from 'fs';

@Singleton
@OnlyInstantiableByContainer
export class FindService {

    private static CHECK_DISKS_INTERVAL: number = 1000;
    private static SCSI_SCAN_INTERVAL: number = 3000;

    @Inject
    private readonly hardwareInfoService: SystemInfoService;

    @Inject
    private readonly scsiScanService: ScsiScanService;

    @Inject
    private readonly configService: ConfigService;

    public async findDeviceConfigs(): Promise<DeviceConfig[]> {
        const devices: DeviceConfig[] = await this.configService.get('devices');
        console.log('devices',devices);
        return ArrayAsyncUtil.filter(devices,
            async (deviceConfig: DeviceConfig) => {
                if (deviceConfig.enabled === false) return false;
                if (!deviceConfig.hardware) return true;
                const hwConfig: HardwareConfig = deviceConfig.hardware;
                return this.filterHardwareConfigForThisComputer(hwConfig);
            }
        );
    }

    public async findSize(path: string): Promise<number | undefined> {
        try {
            const stats: Stats = await fsPromises.stat(path);
            // console.log(path, stats.isFile(), stats.isBlockDevice());
            if (stats.isFile()) return stats.size;
            if (!stats.isBlockDevice()) return undefined;
            const partInfo: DiskPartitionInfo = await this.findPartitionByPath(path);
            if (partInfo) return partInfo.size;
            const diskInfo: DiskInfo = await this.findDiskByPath(path);
            if (diskInfo) return diskInfo.size;
        } catch (e) { }
        return undefined;
    }

    public async findDiskDevicePath(path: string): Promise<string | undefined> {
        try {
            const diskInfo: DiskInfo = await this.findDiskByPath(path);
            return diskInfo?.path;
        } catch (e) { }
        return undefined;
    }

    public async findDiskByPath(path: string): Promise<DiskInfo | undefined> {
        const disk: DiskInfo = await this.findDiskByDevicePath(path);
        if (disk !== undefined) return disk;
        let pathFromDf: string = await ProcessUtil.exec(
            `realpath $(df ${path} | grep '^/' | cut -d' ' -f1)`
        );
        pathFromDf = pathFromDf.replace(/[\n\r ]/g, '');
        // console.log('pathFromDf',pathFromDf);
        return this.findDiskByDevicePath(pathFromDf);
    }

    public async findPartitionByPath(path: string): Promise<DiskPartitionInfo | undefined> {
        const partitionInfo: DiskPartitionInfo = await this.findPartitionByDevicePath(path);
        if (partitionInfo !== undefined) return partitionInfo;
        try {
            let pathFromDf: string = await ProcessUtil.exec(
                `realpath $(df ${path} | grep '^/' | cut -d' ' -f1)`
            );
            pathFromDf = pathFromDf.replace(/[\n\r ]/g, '');
            return this.findPartitionByDevicePath(pathFromDf);
        } catch (e) { }
        return undefined;
    }

    public async findDiskByDevicePath(devicePath: string): Promise<DiskInfo | undefined> {
        const disks: DiskInfo[] = await this.hardwareInfoService.getDisks(true);
        return disks.find((diskInfo: DiskInfo) => {
            if (diskInfo.path === devicePath) return true;
            const partition: DiskPartitionInfo = diskInfo.partitions.find((partInfo: DiskPartitionInfo) => {
                return partInfo.path === devicePath;
            });
            return partition !== undefined;
        });
    }

    public async findPartitionByDevicePath(devicePath: string): Promise<DiskPartitionInfo | undefined> {
        const disks: DiskInfo[] = await this.hardwareInfoService.getDisks(true);
        for (const diskInfo of disks) {
            for (const partInfo of diskInfo.partitions) {
                if (partInfo.path === devicePath) return partInfo;
            }
        }
        return undefined;
    }

    public async findDiskByConfig(findDiskConfig: FindDiskConfig): Promise<DiskInfo | undefined> {
        if (findDiskConfig.waitForDisk !== true) return this.findDiskByConfigInternal(findDiskConfig);
        if (findDiskConfig.scanScsi === true) this.scsiScanService.startScan(FindService.SCSI_SCAN_INTERVAL);
        const disk: DiskInfo = await AsyncLoopUtil.untilNotUndefined<DiskInfo>(
            this.findDiskByConfigInternal.bind(this, findDiskConfig),
            FindService.CHECK_DISKS_INTERVAL
        );
        if (findDiskConfig.scanScsi === true) await this.scsiScanService.kill();
        return disk;
    }

    public async findPartitionByConfig(findPartitionConfig: FindPartitionConfig): Promise<DiskPartitionInfo | undefined> {
        if (findPartitionConfig.waitForDisk !== true) return this.findPartitionByConfigInternal(findPartitionConfig);
        if (findPartitionConfig.scanScsi === true) this.scsiScanService.startScan(FindService.SCSI_SCAN_INTERVAL);
        const partitionInfo: DiskPartitionInfo = await AsyncLoopUtil.untilNotUndefined<DiskInfo>(
            this.findPartitionByConfigInternal.bind(this, findPartitionConfig),
            FindService.CHECK_DISKS_INTERVAL
        );
        if (findPartitionConfig.scanScsi === true) await this.scsiScanService.kill();
        return partitionInfo;
    }

    public async findPartitionByConfigInternal(findPartitionConfig: FindPartitionConfig): Promise<DiskPartitionInfo | undefined> {
        // console.log('findPartitionBy', findPartitionConfig);
        const partitions: DiskPartitionInfo[] = await this.hardwareInfoService.getDiskPartitions();
        // console.log('partitions', partitions);
        return partitions.find((partition: DiskPartitionInfo) => {
            let hasMinOnePropertyDefined: boolean = false;
            if (findPartitionConfig.uuid) {
                hasMinOnePropertyDefined = true;
                if (findPartitionConfig.uuid.trim() !== partition.uuid?.trim()) return false;
            }
            if (findPartitionConfig.partUuid) {
                hasMinOnePropertyDefined = true;
                if (findPartitionConfig.partUuid.trim() !== partition.partUuid?.trim()) return false;
            }
            if (findPartitionConfig.label) {
                hasMinOnePropertyDefined = true;
                if (findPartitionConfig.label.trim() !== partition.label?.trim()) return false;
            }
            if (findPartitionConfig.partLabel) {
                hasMinOnePropertyDefined = true;
                if (findPartitionConfig.partLabel.trim() !== partition.partLabel?.trim()) return false;
            }
            if (findPartitionConfig.type) {
                hasMinOnePropertyDefined = true;
                if (findPartitionConfig.type.trim() !== partition.type?.trim()) return false;
            }
            if (findPartitionConfig.blockSize) {
                hasMinOnePropertyDefined = true;
                const blockSize: number = BytesUtil.toBytes(findPartitionConfig.blockSize);
                if (blockSize !== partition.blockSize) return false;
            }
            return hasMinOnePropertyDefined;
        });
    }

    private async findDiskByConfigInternal(findDiskConfig: FindDiskConfig): Promise<DiskInfo | undefined> {
        const disks: DiskInfo[] = await this.hardwareInfoService.getDisks(false);
        // console.log('disks', disks);
        return disks.find((disk: DiskInfo) => {
            let hasMinOnePropertyDefined: boolean = false;
            if (findDiskConfig.serial) {
                hasMinOnePropertyDefined = true;
                if (findDiskConfig.serial.trim() !== disk.serial.trim()) return false;
            }
            if (findDiskConfig.vendor) {
                hasMinOnePropertyDefined = true;
                if (findDiskConfig.vendor.trim() !== disk.vendor.trim()) return false;
            }
            if (findDiskConfig.model) {
                hasMinOnePropertyDefined = true;
                if (findDiskConfig.model.trim() !== disk.model.trim()) return false;
            }
            if (findDiskConfig.size) {
                hasMinOnePropertyDefined = true;
                const size: number = BytesUtil.toBytes(findDiskConfig.size);
                if (size !== disk.size) return false;
            }
            return hasMinOnePropertyDefined;
        });
    }

    private async filterHardwareConfigForThisComputer(hwConfig: HardwareConfig): Promise<boolean> {
        if (!hwConfig.mainboard) throw new Error('mainboard undefined');
        if (!hwConfig.cpu) throw new Error('cpu undefined');

        const mainboardInfo: MainboardInfo = await this.hardwareInfoService.getMainboard();
        if (hwConfig.mainboard.trim() !== mainboardInfo.model.trim()) return false;

        const cpuInfo: CpuInfo = await this.hardwareInfoService.getCpu();
        if (hwConfig.cpu.trim() !== cpuInfo.model.trim()) return false;

        if (hwConfig.graphicsAdapters) {
            const graphicsAdapterInfos: GraphicsAdapterInfo[] = await this.hardwareInfoService.getGraphicsAdapaters();
            for (const graphicsAdapter of hwConfig.graphicsAdapters) {
                const graphicsAdapterInfo: GraphicsAdapterInfo = graphicsAdapterInfos.find(
                    (adapterInfo: GraphicsAdapterInfo) => {
                        return graphicsAdapter.trim() === adapterInfo.model.trim();
                    }
                );
                if (!graphicsAdapterInfo) return false;
            }
        }
        return true;
    }

}
