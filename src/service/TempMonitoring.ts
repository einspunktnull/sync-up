import {Inject} from 'typescript-ioc';
import {HddtempService} from './hddtemp/HddtempService';
import {FindService} from './find/FindService';
import {Killable} from '../lib/Killable';
import {LogService} from './log/LogService';

interface ProtectionParams {
    path: string;
    minTemp: number;
    maxTemp: number;
    interval?: string | number;
}

interface Sub {
    uuid: string;
    minTemp: number;
    maxTemp: number;
}

type PauseOrResumeFunction = (device: string, temperature?: number) => Promise<void>;

export abstract class TempMonitoring implements Killable {

    private static readonly DEFAULT_TEMP_INTERVAL_MS: string = '10s';

    @Inject
    protected readonly logService: LogService;

    @Inject
    protected readonly findService: FindService;

    @Inject
    private readonly hddtempService: HddtempService;

    private readonly subs: Map<string, Sub> = new Map<string, Sub>();
    private readonly paramses: ProtectionParams[];
    private isRunning: boolean = false;
    private pauseOrResume: PauseOrResumeFunction;

    protected abstract execute(): Promise<void>;

    protected abstract pause(device: string): Promise<void>;

    protected abstract resume(device: string): Promise<void>;

    protected constructor(paramses: ProtectionParams[]) {
        this.paramses = paramses;
    }

    public async kill(): Promise<void> {
        throw new Error('not implemented');
    }

    public async run(): Promise<void> {
        if (this.isRunning) throw new Error('already running');
        this.isRunning = true;
        await this.enable();
        await this.execute();
        await this.disable();
    }

    private async enable(): Promise<void> {
        this.paramses.forEach((params: ProtectionParams) => {
            if (params.minTemp === undefined || params.maxTemp === undefined) return;
            if (this.subs.has(params.path)) {
                this.logService.warn(this, `temp already monitored: ${params.path}`);
                return;
            }
            const subUuid: string = this.hddtempService.subscribe(
                params.path,
                params.interval || TempMonitoring.DEFAULT_TEMP_INTERVAL_MS,
                this.checkTemperature.bind(this)
            );
            this.subs.set(params.path, {
                uuid: subUuid,
                minTemp: params.minTemp,
                maxTemp: params.maxTemp
            });
        });
    }

    private async disable(): Promise<void> {
        this.subs.forEach((sub: Sub) => this.hddtempService.unsubscribe(sub.uuid));
    }

    private checkTemperature(device: string, temperature: number): void {
        this.logService.info(device, temperature);
        if (this.pauseOrResume !== undefined) return;
        const sub: Sub = this.subs.get(device);
        if (temperature > sub.maxTemp) {
            this.pauseOrResume = this.pause.bind(this);
        } else if (temperature <= sub.minTemp) {
            this.pauseOrResume = this.resume.bind(this);
        }
        if (this.pauseOrResume)
            this.pauseOrResume(device, temperature)
                .then(() => this.pauseOrResume = undefined)
                .catch(this.logService.error.bind(this.logService, this));
    }

}
