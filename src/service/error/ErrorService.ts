import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {SyncUpError} from '../../core/SyncUpError';
import {ExitCode} from '../../core/ExitCode';
import {LogService} from '../log/LogService';

@Singleton
@OnlyInstantiableByContainer
export class ErrorService {

    @Inject
    private readonly logger: LogService;

    public addError(error: Error, ...args): SyncUpError {
        const syncUpError: SyncUpError = new SyncUpError(
            error.message || 'Unknown Error',
            error,
            args
        );
        this.logger.error(syncUpError);
        return syncUpError;
    }

    public addErrorAndKillApp(error: Error, exitCode: number, ...args): SyncUpError {
        const syncUpError: SyncUpError = this.addError.apply(this, [error, ...args]);
        this.killApp(exitCode);
        return syncUpError;
    }

    private killApp(exitCode: number = ExitCode.UNKNOWN_ERROR): void {
        process.exit(exitCode);
    }
}
