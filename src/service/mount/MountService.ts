import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {Mount, MountEntry} from '../../lib/mount/Mount';
import {ConfigService} from '../config/ConfigService';
import {FsUtil} from '@beautils/all';
import mkdirp from 'mkdirp';
import {v1 as uuidV1} from 'uuid';
import path from 'path';
import {UndoService} from '../undo/UndoService';
import {UndoType} from '../undo/types';
import {MountDir, StorageDir} from '../../core/constants';
import {SyncUpConfig} from '../../core/config/SyncUpConfig';

@Singleton
@OnlyInstantiableByContainer
export class MountService {

    private readonly _mount: Mount = new Mount();

    @Inject
    private readonly configService: ConfigService;

    @Inject
    private readonly undoService: UndoService;

    private mountDirExists: boolean = false;

    public async mount(partitionPath: string): Promise<string> {
        let mountPoint: string = await this.getMountPoint(partitionPath);
        if (mountPoint) return mountPoint;
        mountPoint = await this.createMountPoint();
        await this.mountPartition(partitionPath, mountPoint);
        return mountPoint;
    }

    private async mountPartition(partition: string, mountPoint: string): Promise<void> {
        await this._mount.mountPartition(partition, mountPoint);
        return this.undoService.addUndo(UndoType.mount, partition);
    }

    private async getMountPoint(partitionPath: string): Promise<string | undefined> {
        // console.log('partitionPath',partitionPath);
        const mounts: MountEntry[] = await this._mount.list();
        return mounts.find(mountEntry => mountEntry.target === partitionPath)?.mountPoint;
    }

    private async createMountPoint(): Promise<string> {
        const baseDir: string = await this.getBaseDir();
        const mountPoint: string = await this.randomDirname(baseDir);
        if (!await FsUtil.exists(mountPoint)) {
            await mkdirp(mountPoint);
            await this.undoService.addUndo(UndoType.mkdir, mountPoint);
        }
        // console.log('mountPoint', mountPoint);
        return mountPoint;
    }

    private async getBaseDir(): Promise<string> {
        const config:SyncUpConfig = await this.configService.get()
        const dirPath: string = path.resolve(
            config.app.dataDir + path.sep + MountDir
        );
        if (this.mountDirExists) return dirPath;
        if (await FsUtil.exists(dirPath)) return dirPath;
        await mkdirp(dirPath);
        this.mountDirExists = true;
        return dirPath;
    }

    private async randomDirname(rootDir: string = undefined): Promise<string> {
        if (rootDir === undefined) rootDir = path.parse(process.cwd()).root;
        rootDir = path.resolve(rootDir);
        // console.log('rootDir', rootDir);
        if (!await FsUtil.exists(rootDir)) throw new Error('path does not exist: ' + rootDir);
        if (!(await FsUtil.lstat(rootDir)).isDirectory()) throw new Error('path is no dir: ' + rootDir);
        while (true) {
            let start: string = rootDir + path.sep;
            start = start.replace(/[\/\\]{2,}/, path.sep);
            const randomDir: string = start + uuidV1();
            if (!await FsUtil.exists(randomDir)) return randomDir;
        }
    }

}
