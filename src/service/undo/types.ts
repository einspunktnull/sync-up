export enum UndoType {
    mkdir = 'mkdir',
    mount = "mount"
}

export interface Undo {
    type: UndoType;
    data: any;
}
