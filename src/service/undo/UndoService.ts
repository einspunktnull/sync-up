import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {Undo, UndoType} from './types';
import {FileUtil} from '@beautils/all';
import {Mount} from '../../lib/mount/Mount';
import {StorageService} from '../storage/StorageService';
import {LogService} from '../log/LogService';

@Singleton
@OnlyInstantiableByContainer
export class UndoService {

    @Inject
    private readonly storageService: StorageService;

    @Inject
    private readonly logService: LogService;

    private readonly undoKeys: string[] = [];

    public async addUndo(type: UndoType, data: any): Promise<void> {
        const undo: Undo = {
            data: data,
            type: type
        };
        const key: string = await this.storageService.set(undo);
        this.undoKeys.push(key);
    }

    public async execute(): Promise<void> {
        const undoKeys: string[] = this.undoKeys.reverse();
        for (const undoKey of undoKeys) await this.undo(undoKey);
        await this.storageService.delete(undoKeys);
    }

    private async undo(undoKey: string): Promise<string | void> {
        const undo: Undo | undefined = await this.storageService.get(undoKey);
        this.logService.info(undo?.type, undo?.data);
        switch (undo?.type) {
            case UndoType.mkdir:
                return FileUtil.remove(undo.data);
            case UndoType.mount:
                return new Mount().unmountPartition(undo.data);
        }
        throw new Error('unknown undo type: ' + undo?.type);
    }
}
