import {Systeminformation} from 'systeminformation';

export type SysteminformationStaticData = Systeminformation.StaticData;

export interface HardwareInfo {
    cpu: CpuInfo;
    mainboard: MainboardInfo;
    graphicsAdapters: GraphicsAdapterInfo[];
    disks: DiskInfo[];
}

export interface ComponentInfo {
    model: string;
    manufacturer?: string;
    vendor?: string;
}

export interface CpuInfo extends ComponentInfo {
    cores: number;
}

export interface MainboardInfo extends ComponentInfo {
}

export interface GraphicsAdapterInfo extends ComponentInfo {
}

export interface DiskInfo extends ComponentInfo {
    path: string;
    size: number;
    serial: string;
    partitions?: DiskPartitionInfo[];
}

export interface DiskPartitionInfo {
    path: string;
    size: number;
    uuid?: string;
    partUuid?: string;
    label?: string;
    partLabel?: string;
    type?: string;
    blockSize?: number;
}

export interface SystemSummary {
    hardware: HardwareInfo;
    systeminformationStaticData: SysteminformationStaticData;
}
