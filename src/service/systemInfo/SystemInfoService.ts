import {OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import systeminformation, {Systeminformation} from 'systeminformation';
import {
    SystemSummary,
    CpuInfo,
    DiskInfo,
    DiskPartitionInfo,
    GraphicsAdapterInfo,
    HardwareInfo,
    MainboardInfo,
    SysteminformationStaticData
} from './types';
import {Blkid, BlkidPartition} from '../../lib/blkid/Blkid';
import {ArrayAsyncUtil, ProcessUtil} from '@beautils/all';

@Singleton
@OnlyInstantiableByContainer
export class SystemInfoService {

    public async getSysteminformationStaticData(): Promise<SysteminformationStaticData> {
        return systeminformation.getStaticData();
    }

    public async getSummary(): Promise<SystemSummary> {
        return {
            hardware: await this.getHardware(),
            systeminformationStaticData: await this.getSysteminformationStaticData(),
        };
    }

    public async getHardware(): Promise<HardwareInfo> {
        return {
            mainboard: await this.getMainboard(),
            graphicsAdapters: await this.getGraphicsAdapaters(),
            disks: await this.getDisks(),
            cpu: await this.getCpu(),
        };
    }

    public async getMainboard(): Promise<MainboardInfo> {
        const baseboardData: Systeminformation.BaseboardData = await systeminformation.baseboard();
        return {
            manufacturer: baseboardData.manufacturer,
            model: baseboardData.model
        };
    }

    public async getCpu(): Promise<CpuInfo> {
        const cpuData: Systeminformation.CpuData = await systeminformation.cpu();
        return {
            manufacturer: cpuData.manufacturer,
            vendor: cpuData.vendor,
            model: cpuData.brand,
            cores: cpuData.cores,
        };
    }

    public async getGraphicsAdapaters(): Promise<GraphicsAdapterInfo[]> {
        const graphicsData: Systeminformation.GraphicsData = await systeminformation.graphics();
        return graphicsData.controllers.map((controllerData: Systeminformation.GraphicsControllerData) => {
            return {
                model: controllerData.model,
                vendor: controllerData.vendor
            };
        });
    }

    public async getDisk(devicePath: string): Promise<DiskInfo> {
        const diskInfos: DiskInfo[] = await this.getDisks();
        const diskInfo: DiskInfo = diskInfos.find(di => di.path === devicePath);
        if (diskInfo === undefined) throw new Error('no device ' + devicePath + ' found');
        return diskInfo;
    }

    public async getDisks(listPartitions: boolean = true): Promise<DiskInfo[]> {
        const diskLayoutDatas: Systeminformation.DiskLayoutData[] = await systeminformation.diskLayout();
        return ArrayAsyncUtil.map<DiskInfo>(diskLayoutDatas, async (diskLayoutData: Systeminformation.DiskLayoutData) => {
            const devicePath: string = diskLayoutData.device;
            const result: DiskInfo = {
                path: devicePath,
                vendor: diskLayoutData.vendor,
                model: diskLayoutData.name,
                serial: diskLayoutData.serialNum,
                size: diskLayoutData.size,
            };
            if (listPartitions) result.partitions = await this.getDiskPartitions(devicePath);
            return result;
        });
    }

    public async getDiskPartitions(devicePath?: string): Promise<DiskPartitionInfo[]> {
        const allPartitions: BlkidPartition[] = await Blkid.exec();
        // console.log('allPartitions', allPartitions);
        const devicePartitions: BlkidPartition[] = devicePath ?
            allPartitions.filter(p => p.path.search(devicePath) === 0) :
            allPartitions;
        // console.log('devicePartitions', devicePartitions);

        return ArrayAsyncUtil.map<DiskPartitionInfo>(devicePartitions, async (p: BlkidPartition) => {
            const path:string = p.path;
            try {
                const sizeStr: string = (await ProcessUtil.spawn('lsblk',[
                    '-bno',
                    'SIZE',
                    path,
                ])).replace(/[\n\r]/g, '');
                return {
                    path: path,
                    size: parseInt(sizeStr),
                    label: p.LABEL,
                    partLabel: p.PARTLABEL,
                    uuid: p.UUID,
                    partUuid: p.PARTUUID,
                    blockSize: p.BLOCK_SIZE ? parseInt(p.BLOCK_SIZE, 10) : undefined,
                    type: p.TYPE,
                };
            } catch (e) {
                return undefined;
            }
        });

    }

}

