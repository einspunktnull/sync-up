import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {v1 as uuidV1} from 'uuid';
import {DateAndTimeUtil} from '@beautils/all';
import {HddTempCallback, HddtempWatcher} from '../../lib/hddtemp/HddtempWatcher';
import {Killable} from '../../lib/Killable';
import {LogService} from '../log/LogService';

@Singleton
@OnlyInstantiableByContainer
export class HddtempService implements Killable {

    @Inject
    private readonly logService: LogService;

    private readonly watchers: Map<string, HddtempWatcher> = new Map<string, HddtempWatcher>();

    public subscribe(disk: string, interval: string | number, callback: HddTempCallback): string {
        // console.log('subscribe', disk, interval);
        const intervalMs: number = DateAndTimeUtil.toMilliseconds(interval);
        const watcher: HddtempWatcher = new HddtempWatcher(disk, intervalMs, callback);
        const subUuid: string = uuidV1();
        this.watchers.set(subUuid, watcher);
        return subUuid;
    }

    public unsubscribe(subUuid: string): void {
        // console.log('unsubscribe', subUuid);
        const watcher: HddtempWatcher = this.watchers.get(subUuid);
        if (!watcher) return;
        watcher.kill();
        this.watchers.delete(subUuid);
    }

    public kill(): void {
        this.watchers.forEach((watcher: HddtempWatcher) => {
            try {
                watcher.kill();
            } catch (e) {
                this.logService.error(e);
            }
        });
    }

}
