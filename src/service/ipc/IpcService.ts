import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {Ipc} from '../../core/ipc/Ipc';
import {ProcessInfoService} from '../processInfo/ProcessInfoService';
import {CreateAnswerValueFct, Event} from '../../core/ipc/types';
import {Initializable} from '../../lib/Initializable';

@Singleton
@OnlyInstantiableByContainer
export class IpcService extends Initializable<void> {

    @Inject
    protected readonly processInfoService: ProcessInfoService;

    private ipc: Ipc;

    protected async initialize(): Promise<void> {
        // console.log('IpcService.initialize');
        this.ipc = await Ipc.create(
            this.processInfoService.motherProcessId,
            this.processInfoService.id
        );
    }

    public answer<Tin, Tout>(event: Event, answer: CreateAnswerValueFct<Tin, Tout>): void {
        // console.log('answer',this.expectInitialized);
        return this.expectInitialized.ipc.answer(event, answer);
    }

    public ask<Tout, Tin>(event: Event, data?: Tout): Promise<Tin> {

        return this.expectInitialized.ipc.ask(event, data);
    }

}
