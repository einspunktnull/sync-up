import {Inject, OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {ConfigService} from '../config/ConfigService';
import path from 'path';
import {FsUtil, JsonUtil, PromiseUtil} from '@beautils/all';
import {v1 as uuidV1} from 'uuid';
import mkdirp from 'mkdirp';
import fsPromises from 'fs/promises';
import {StorageDir} from '../../core/constants';
import {isArray, isBoolean, isString} from 'lodash';
import {SyncUpConfig} from '../../core/config/SyncUpConfig';

interface JsonWrap {value: any}

@Singleton
@OnlyInstantiableByContainer
export class StorageService {

    @Inject
    private readonly configService: ConfigService;

    private readonly cache: Map<string, any> = new Map<string, any>();
    private storageDirExists: boolean = false;

    public async set(key: string, value: any): Promise<string>;
    public async set(value: any): Promise<string>;
    public async set(...args: any): Promise<string> {
        const key: string = args.length === 1 ? this.createKey() : args[0];
        const value: any = args.length === 1 ? args[0] : args[1];
        await this.saveToFile(key, value);
        this.cache.set(key, value);
        return key;
    }

    public async get<T>(key: string): Promise<T | undefined> {
        if (this.cache.has(key)) return this.cache.get(key);
        try {
            const value: T = await this.readFromFile<T>(key);
            this.cache.set(key, value);
            return value;
        } catch (e) {
            return undefined;
        }
    }

    public async delete(keyOrKeys: string | string[]): Promise<void> {
        if (isString(keyOrKeys)) keyOrKeys = [keyOrKeys];
        await PromiseUtil.queueForEach(keyOrKeys as string[], async (key: string) => {
            const filePath: string = await this.getFilePath(key);
            await this.deleteFile(filePath);
            this.cache.delete(key);
        }, Infinity);
    }

    public async clear(): Promise<void>; // all from cache
    public async clear(keys: string[]): Promise<void>; // all given
    public async clear(all: boolean): Promise<void>; // all from cache and existing
    public async clear(keysOrAll?: string[] | boolean): Promise<void> {
        if (isArray(keysOrAll)) return await this.delete(keysOrAll);
        await this.delete(Array.from(this.cache.keys()));
        if (keysOrAll !== true) return;
        const files: string[] = await fsPromises.readdir(await this.getStorageDir());
        await PromiseUtil.queueForEach(files, async (file: string) => {
            const filePath: string = await this.getStorageDir() + path.sep + file;
            return this.deleteFile(filePath);
        }, Infinity);
    }

    public createKey(): string {
        return uuidV1();
    }

    private async saveToFile(key: string, value: any): Promise<void> {
        const filePath: string = await this.getFilePath(key);
        const wrap: JsonWrap = {value: value};
        return JsonUtil.writeJsonFile(wrap, filePath);
    }

    private async readFromFile<T>(key: string): Promise<T> {
        const filePath: string = await this.getFilePath(key);
        const wrap: JsonWrap = await JsonUtil.readJsonFile(filePath);
        return wrap.value;
    }

    private async deleteFile(filePath: string): Promise<void> {
        await fsPromises.unlink(filePath);
    }

    private async getFilePath(key: string): Promise<string> {
        return await this.getStorageDir() + path.sep + key + '.json';
    }

    private async getStorageDir(): Promise<string> {
        const config:SyncUpConfig = await this.configService.get();
        const dirPath: string = path.resolve(
            config.app.dataDir + path.sep + StorageDir
        );
        if (this.storageDirExists) return dirPath;
        if (await FsUtil.exists(dirPath)) return dirPath;
        await mkdirp(dirPath);
        this.storageDirExists = true;
        return dirPath;
    }
}
