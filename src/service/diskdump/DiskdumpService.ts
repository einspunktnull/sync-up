import {Diskdump, DiskdumpProgress, DiskdumpState} from '../../lib/diskdump/Diskdump';
import {DiskProtectionConfig} from '../../core/config/SyncUpConfig';
import {Killable} from '../../lib/Killable';
import {TempMonitoring} from '../TempMonitoring';
import {StandbyService} from '../standby/StandbyService';
import {Inject} from 'typescript-ioc';

interface DumpFile {
    path: string;
    diskPath: string;
    diskProtection?: DiskProtectionConfig,
}

export class DiskdumpService extends TempMonitoring implements Killable {

    @Inject
    private readonly standbyService: StandbyService;

    private readonly diskdump: Diskdump;
    private readonly input: DumpFile;
    private readonly output: DumpFile;

    public constructor(input: DumpFile, output: DumpFile) {
        super([
            {
                path: input.diskPath,
                interval: input.diskProtection?.interval,
                minTemp: input.diskProtection?.minTemp,
                maxTemp: input.diskProtection?.maxTemp,
            },
            {
                path: output.diskPath,
                interval: output.diskProtection?.interval,
                minTemp: output.diskProtection?.minTemp,
                maxTemp: output.diskProtection?.maxTemp,
            }
        ]);
        this.input = input;
        this.output = output;
        this.diskdump = new Diskdump(input.path, output.path);
    }

    public async kill(): Promise<void> {
        try {
            await this.diskdump.kill();
        } catch (e) {
            this.logService.error(e);
        }
    }

    protected async execute(): Promise<void> {
        const size: number = await this.findService.findSize(this.input.path);
        this.diskdump.progress.subscribe((progress: DiskdumpProgress) => {
            const written: number = progress.writtenBytes;
            const percent: number = Math.floor(written / size * 100);
            const progressStr: string = `${percent}% (${written} of ${size})`;
            this.logService.info('diskdump progress', progressStr);
        });
        this.diskdump.stateChange.subscribe((state: DiskdumpState) => {
            this.logService.info('diskdump state', state);
        });
        await this.diskdump.execute();
    }

    protected async pause(device: string): Promise<void> {
        if (!this.diskdump.isPausable) return;
        // console.log('DiskdumpService.pause',device);
        if (this.getDumpFile(device).diskProtection.standBy)
            await this.standbyService.forceStandby(device);
        return this.diskdump.pause();
    }

    protected async resume(device: string): Promise<void> {
        if (!this.diskdump.isResumable) return;
        // console.log('DiskdumpService.resume',device);
        if (this.getDumpFile(device).diskProtection.standBy)
            await this.standbyService.stop(device);
        return this.diskdump.resume();
    }

    private getDumpFile(diskPath: string): DumpFile | null {
        if (this.input.diskPath === diskPath) return this.input;
        if (this.output.diskPath === diskPath) return this.output;
        return null;
    }

}
