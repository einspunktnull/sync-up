import child_process, {ChildProcess} from 'child_process';
import {ReplaySubject, Subject} from 'rxjs';

export class DiskdumpError extends Error {
    constructor(
        public readonly message: string,
        public readonly origError?: any,
        public readonly exitCode?: number,
        public readonly data?: any
    ) { super(message); }
}

export interface DiskdumpProgress {
    writtenRecords: number;
    writtenBytes: number;
}

export enum DiskdumpState {
    ZERO = 'ZERO',
    ERRORED = 'ERRORED',
    STARTING = 'STARTING',
    STARTED = 'STARTED',
    PAUSING = 'PAUSING',
    PAUSED = 'PAUSED',
    RESUMING = 'RESUMING',
    RESUMED = 'RESUMED',
    FINISHED = 'FINISHED',
    KILLING = 'KILLING',
    KILLED = 'KILLED',
}

type ResolveFct<T> = (value: (PromiseLike<T> | T)) => void;
type RejectFct = (reason?: any) => void;

export class Diskdump {

    public static readonly COMMAND: string = 'dd';

    private static readonly EXITCODE_SUCCESS: number = 0;
    private static readonly EXITCODE_NULL: null = null;
    private static readonly PROGRESS_SIGNAL: NodeJS.Signals = 'SIGUSR1';
    private static readonly TERMINATION_SIGNAL: NodeJS.Signals = 'SIGTERM';
    private static readonly KILL_SIGNAL: NodeJS.Signals = 'SIGKILL';
    private static readonly PROGRESS_INTERVAL: number = 1000;
    private static readonly REGEX_RECORDS: RegExp = /(\d+)\+\d+ records in\n\r?(\d+)\+\d+ records out/;

    public readonly progress: Subject<DiskdumpProgress> = new Subject<DiskdumpProgress>();
    public readonly stateChange: ReplaySubject<DiskdumpState> = new ReplaySubject<DiskdumpState>();

    private readonly inputFile: string;
    private readonly outputFile: string;
    private readonly blockSize: number;
    private progressInterval: NodeJS.Timeout;
    private unknownDataFromChildProcess: string[] = [];
    private childProcess: ChildProcess;

    private executeResolve: ResolveFct<void>;
    private executeReject: RejectFct;
    private pauseResolve: ResolveFct<void>;
    private killResolve: ResolveFct<void>;

    private _writtenRecordsTotal: number = 0;
    private _writtenRecordsCurrent: number = 0;
    private _state: DiskdumpState;

    constructor(inputFile: string, outputFile: string, blockSize: number = 512, writtenRecords: number = 0) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.blockSize = blockSize;
        this.state = DiskdumpState.ZERO;
        this._writtenRecordsTotal = writtenRecords;
    }

    public get isPausable(): boolean {
        return this.state === DiskdumpState.STARTED || this.state === DiskdumpState.RESUMED;
    }

    public get isResumable(): boolean {
        return this.state === DiskdumpState.PAUSED;
    }

    protected set state(state: DiskdumpState) {
        this._state = state;
        this.stateChange.next(this._state);
    }

    protected get state(): DiskdumpState {
        return this._state;
    }

    public async execute(): Promise<void> {
        // console.log('Diskdump.execute()');
        if (this.state !== DiskdumpState.ZERO)
            throw new DiskdumpError('cannot execute. state is not ZERO');
        this.state = DiskdumpState.STARTING;
        return new Promise<void>(async (resolve: ResolveFct<void>, reject: RejectFct) => {
            this.executeResolve = resolve;
            this.executeReject = reject;
            this.spawnChildProc();
            this.state = DiskdumpState.STARTED;
            this.updateProgress();
        });
    }

    public async resume(): Promise<void> {
        // console.log('Diskdump.resume()');
        if (this.state !== DiskdumpState.PAUSED)
            throw new DiskdumpError('cannot resume. state is not PAUSED');
        this.state = DiskdumpState.RESUMING;
        return new Promise<void>(async (resolve: ResolveFct<void>) => {
            this.spawnChildProc();
            resolve();
            this.state = DiskdumpState.RESUMED;
        });
    }

    public async pause(): Promise<void> {
        // console.log('Diskdump.pause()');
        if (this.state !== DiskdumpState.STARTED && this.state !== DiskdumpState.RESUMED)
            throw new DiskdumpError('cannot pause. state is not STARTED or RESUMED');
        this.state = DiskdumpState.PAUSING;
        return new Promise<void>((resolve: ResolveFct<void>) => {
            this.pauseResolve = resolve;
            clearInterval(this.progressInterval);
            this.childProcess.kill(Diskdump.TERMINATION_SIGNAL);
        });
    }

    public async kill(): Promise<void> {
        // console.log('Diskdump.kill()');
        if (this.state === DiskdumpState.KILLING || this.state === DiskdumpState.KILLED) return;
        this.state = DiskdumpState.KILLING;
        return new Promise<void>((resolve: ResolveFct<void>) => {
            this.killResolve = resolve;
            if (this.childProcess) this.childProcess.kill(Diskdump.KILL_SIGNAL);
            else this.state = DiskdumpState.KILLED;
        });
    }

    private spawnChildProc(): void {
        // console.log('Diskdump.spawnChildProc()');
        const args: string[] = [
            `if=${this.inputFile}`,
            `of=${this.outputFile}`,
            `bs=${this.blockSize}`,
            'status=noxfer',
        ];
        if (this._writtenRecordsTotal > 0) {
            args.push(`seek=${this._writtenRecordsTotal}`);
            args.push(`skip=${this._writtenRecordsTotal}`);
        }
        // console.log(args.join(' '));
        this.childProcess = child_process.spawn(Diskdump.COMMAND, args);
        this.childProcess.stderr.on('data', this.onChildProcData.bind(this));
        this.childProcess.stdout.on('data', this.onChildProcData.bind(this));
        this.childProcess.on('error', this.onChildProcError.bind(this));
        this.childProcess.on('exit', this.onChildProcExit.bind(this));
        this.progressInterval = setInterval(() => {
            this.childProcess.kill(Diskdump.PROGRESS_SIGNAL);
        }, Diskdump.PROGRESS_INTERVAL);
    }

    private onChildProcData(buff: Buffer): void {
        // console.log('Diskdump.onChildProcData()');
        const data: string = buff.toString();
        const matches: string[] = data.match(Diskdump.REGEX_RECORDS);
        if (matches === null) {
            this.unknownDataFromChildProcess.push(data);
            throw new DiskdumpError(
                'data from dd not matching regex',
                this.unknownDataFromChildProcess
            );
        }
        this._writtenRecordsCurrent = parseInt(matches[2]);
        this.updateProgress();
    }

    private onChildProcExit(exitCode: number, signal: NodeJS.Signals): void {
        // console.log('Diskdump.onChildProcExit()', exitCode, signal);
        clearInterval(this.progressInterval);
        this.childProcess = undefined;
        if (exitCode === Diskdump.EXITCODE_SUCCESS) {
            this.executeResolve();
            this.state = DiskdumpState.FINISHED;
            return;
        }
        if (
            exitCode === Diskdump.EXITCODE_NULL &&
            signal === Diskdump.TERMINATION_SIGNAL &&
            this.state === DiskdumpState.PAUSING
        ) {
            const pauseResolve: ResolveFct<void> = this.pauseResolve;
            this._writtenRecordsTotal += this._writtenRecordsCurrent;
            this.pauseResolve = undefined;
            pauseResolve();
            this.state = DiskdumpState.PAUSED;
            return;
        }
        if (this.state === DiskdumpState.KILLING) {
            this.state = DiskdumpState.KILLED;
            return;
        }
        this.onChildProcError(null, exitCode);
    }

    private onChildProcError(origError: Error, exitCode?: number): void {
        // console.log('Diskdump.onChildProcError()', origError);
        this.state = DiskdumpState.ERRORED;
        const error: DiskdumpError = new DiskdumpError(
            'unknown dd child proc error',
            origError,
            exitCode,
            this.unknownDataFromChildProcess
        );
        if (this.executeReject) this.executeReject(error);
        throw error;
    }

    private updateProgress(): void {
        const writtenRecords:number = this._writtenRecordsTotal + this._writtenRecordsCurrent;
        this.progress.next({
            writtenBytes: writtenRecords * this.blockSize,
            writtenRecords: writtenRecords,
        });
    }

}
