import {Hddtemp} from './Hddtemp';

export type HddTempCallback = (devicePath: string, temperature: number) => void;

export class HddtempWatcher {

    private isActive: boolean = false;
    private timeout: NodeJS.Timeout;
    private resolve: (value: (PromiseLike<void> | void)) => void;
    private isfakeTemp: boolean = false;
    private fakeTempInterval: NodeJS.Timeout;

    constructor(
        private readonly disk: string,
        private readonly intervalMs: number,
        private readonly callback: HddTempCallback
    ) {
        this.run();
    }

    public kill(): void {
        this.isActive = false;
        if (this.fakeTempInterval) clearInterval(this.fakeTempInterval);
        if (this.timeout) clearTimeout(this.timeout);
        if (this.resolve) this.resolve();
    }

    private async run(): Promise<void> {
        this.isActive = true;
        this.fakeTempInterval = setInterval(() => {
            console.log('fakeTemp',this.isfakeTemp);
            this.isfakeTemp = !this.isfakeTemp;
        }, 10000);
        while (this.isActive) {
            await this.measure();
        }
    }

    private measure(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            this.resolve = resolve;
            try {
                const value: number = this.isfakeTemp ? 100 : await this.exec();
                if (value !== undefined) this.callback(this.disk, value);
                this.timeout = setTimeout(this.resolve, this.intervalMs);
            } catch (e) {
                reject(e);
            }
        });
    }

    private async exec(): Promise<number | undefined> {
        try {
            return await Hddtemp.exec(this.disk);
        } catch (e) {
            return undefined;
        }
    }

}
