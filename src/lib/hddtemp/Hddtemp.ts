import {Spawn} from '@beautils/all';

export class Hddtemp {
    public static COMMAND: string = 'hddtemp';
    private static REGEX: RegExp = /(-?\d+)°C/;
    private readonly device: string;

    public static async exec(device: string): Promise<number> {
        return new Hddtemp(device).exec();
    }

    public constructor(device: string) {
        this.device = device;
    }

    public async exec(): Promise<number> {
        const spawn: Spawn = new Spawn(Hddtemp.COMMAND, [this.device]);
        const result: string = (await spawn.execute()).join('');
        const matches: string[] = result.match(Hddtemp.REGEX);
        if (!matches) throw new Error(spawn.stderrs.join(''));
        return parseInt(matches[1]);
    }
}
