import path from 'path';

export interface ErrorStack {
    raw: string;
    locations: Array<ErrorLocation>
}

export interface ErrorLocation {
    raw: string;
    at: string;
    filePath: string;
    fileName: string;
    line: number;
    column: number;
}

export class ErrorUtil {

    private static readonly RegExStackLine: RegExp = /^(at (\S+) \((\S+):(\d+):(\d+))\)\n\r?$/;
    private static readonly RegExStackLines: RegExp = /at \S+ \(\S+:\d+:\d+\)\n\r?/g;
    private static readonly RegExErrorUtil: RegExp = new RegExp(ErrorUtil.name);

    public static createErrorStack(error?: Error): ErrorStack {
        return this.getErrorStack(error ?? new Error());
    }

    public static getErrorStack(error: Error): ErrorStack {
        const errorStackStr: string = error?.stack;
        const lines: string[] = errorStackStr.match(this.RegExStackLines)
            .filter(line => !this.RegExErrorUtil.test(line));
        return {
            raw: errorStackStr,
            locations: lines.map((line: string) => {
                const matches: string[] = line.match(this.RegExStackLine);
                // console.log('matches', matches);
                const filePath:string = matches[3];
                const fileName:string = path.basename(filePath);
                return {
                    raw: matches[1],
                    at: matches[2],
                    filePath: filePath,
                    fileName: fileName,
                    line: parseInt(matches[4]),
                    column: parseInt(matches[5]),
                };
            })
        };
    }

}


