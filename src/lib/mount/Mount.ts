import {ProcessUtil} from '@beautils/all';

export interface MountEntry {
    raw: string;
    target: string;
    mountPoint: string;
    type: string;
    options: string[];
}

export class Mount {

    public static readonly COMMAND_MOUNT: string = 'mount';
    public static readonly COMMAND_UMOUNT: string = 'umount';

    public async list(): Promise<MountEntry[]> {
        const result: string = await this.execMount();
        return this.parse(result);
    }

    public async all(...options: string[]): Promise<string> {
        return this.execMount(['-a', ...options]);
    }

    public async mountPartition(source: string, mountPoint: string, type: string = 'auto'): Promise<string> {
        return this.mount(['-t', type], source, mountPoint);
    }

    public async unmountPartition(source: string): Promise<string> {
        return this.unmount(['-l'], source);
    }

    public async mount(options: string [], source: string, mountPoint: string): Promise<string> {
        return this.execMount([...options, source, mountPoint]);
    }

    public async unmount(options: string [], source: string): Promise<string> {
        return this.execUmount([...options, source]);
    }

    public async execMount(args: string[] = []): Promise<string> {
        return this.exec(Mount.COMMAND_MOUNT, args);
    }

    public async execUmount(args: string[] = []): Promise<string> {
        return this.exec(Mount.COMMAND_UMOUNT, args);
    }

    private async exec(cmd: string, args: string[]): Promise<string> {
        return ProcessUtil.spawn(cmd, args);
    }

    private parse(str: string): MountEntry[] {
        // console.log('parse', str);
        const lines: string[] = str.split(/\n\r?/).filter(l => l !== '');
        // console.log('lines', lines);
        return lines.map<MountEntry>(this.mapLine.bind(this));
    }

    private mapLine(line: string): MountEntry {
        // console.log('line', line);
        const regExp: RegExp = /^([^ ]+) on ([^ ]+) type ([^ ]+) \(([^ ]+)\)$/;
        const matches: string[] = line.match(regExp);
        // console.log('matches', matches);
        return {
            raw: matches[0],
            target: matches[1],
            mountPoint: matches[2],
            type: matches[3],
            options: matches[4].split(/,/)
        };
    }

}
