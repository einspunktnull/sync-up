import {ProcessUtil} from '@beautils/all';

export interface BlkidPartition {
    path: string;
    UUID: string;
    PARTUUID: string;
    BLOCK_SIZE: string;
    TYPE: string;
    LABEL: string;
    PARTLABEL: string;
}

export class Blkid {

    public static readonly COMMAND: string = 'blkid';

    public static async exec(): Promise<BlkidPartition[]> {
        return new Blkid().exec();
    }

    private constructor() { }

    private async exec(): Promise<BlkidPartition[]> {
        const result: string = await ProcessUtil.spawn(Blkid.COMMAND);
        return this.parse(result);
    }

    private parse(str: string): BlkidPartition[] {
        // console.log('parse', str);
        const lines: string[] = str.split(/\n\r?/).filter(l => l !== '');
        // console.log('lines', lines);
        return lines.map<BlkidPartition>(this.mapLine.bind(this));
    }

    private mapLine(line: string): BlkidPartition {
        // console.log('line', line);
        const pathRegex: RegExp = /^(\/dev\/[0-9a-z]+)/;
        const path: string = line.match(pathRegex)[0];
        return {
            path: path,
            UUID: this.matchProp(line, 'UUID'),
            PARTUUID: this.matchProp(line, 'PARTUUID'),
            BLOCK_SIZE: this.matchProp(line, 'BLOCK_SIZE'),
            TYPE: this.matchProp(line, 'TYPE'),
            LABEL: this.matchProp(line, 'LABEL'),
            PARTLABEL: this.matchProp(line, 'PARTLABEL'),
        };
    }

    private matchProp(line: string, prop: string): string | undefined {
        const regex: RegExp = new RegExp(prop + '="([^"]+)"');
        const matches: string[] = line.match(regex);
        return matches ? matches[1] : undefined;
    }
}
