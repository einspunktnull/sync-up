export interface Killable {
    kill(): void | Promise<void>;
}
