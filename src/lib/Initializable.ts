import {expect} from '@beautils/all';

export abstract class Initializable<T> {

    private _isInitialized: boolean = false;
    private _isInitializing: boolean = false;

    protected abstract initialize(...args: any[]): T | Promise<T>;

    public initSync(...args: any[]): T {
        this._before();
        const result: T = <T>this.initialize(...args);
        this._after();
        return result;
    }

    public async initAsync(...args: any[]): Promise<T> {
        this._before();
        const result: T = await this.initialize(...args);
        this._after();
        return result;
    }

    private _before(): void {
        this.expectNotInitialized.expectNotInitializing;
        this._isInitializing = true;
    }

    private _after(): void {
        this._isInitializing = false;
        this._isInitialized = true;
    }

    protected get isInitialized(): boolean {
        return this._isInitialized;
    }

    protected get expectInitialized(): this {
        expect(this.isInitialized).isTrue('not initialized');
        return this;
    }

    private get expectNotInitialized(): this {
        expect(this.isInitialized).isFalse('already initialized');
        return this;
    }

    private get expectNotInitializing(): this {
        expect(this._isInitializing).isFalse('already initialzing');
        return this;
    }

}
