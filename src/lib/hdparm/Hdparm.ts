import {ProcessUtil} from '@beautils/all';
import os from 'os';

export enum DriveState {
    ACTIVE_IDLE = 'active/idle',
    STANDBY = 'standby',
    SLEEPING = 'sleeping',
}

export class Hdparm {

    public static readonly COMMAND: string = 'hdparm';
    private static readonly COMMAND_ARG_DRIVE_STATE: string = '-C';
    private static readonly COMMAND_ARG_STANDBY: string = '-y';
    private static readonly REGEX_DRIVE_STATE: RegExp = /drive state is: {2}(\S+)/;
    private readonly device: string;
    private isBusy: boolean = false;

    public constructor(device: string) {
        this.device = device;
    }

    public async standby(): Promise<void> {
        this.setBusy();
        try {
            const state: DriveState = await this.getDriveState();
            // console.log('state', state);
            if (state !== DriveState.ACTIVE_IDLE) throw new Error(
                `cannot enter standby for: ${this.device}${os.EOL}current state is: ${state}`
            );
            await ProcessUtil.spawn(Hdparm.COMMAND, [
                Hdparm.COMMAND_ARG_STANDBY,
                this.device
            ]);
            const stateAfter: DriveState = await this.getDriveState();
            // console.log('stateAfter', stateAfter);
            if (stateAfter !== DriveState.STANDBY) throw new Error(
                `was not able to enter standby: ${this.device}${os.EOL}reason unknown.`
            );
        } catch (e) {
            this.setUnbusy();
            throw new Error(`hdparm reported: ${e.message ? e.message : e}`);
        }
        this.setUnbusy();
    }

    public async getDriveState(): Promise<DriveState> {
        const result: string = await ProcessUtil.spawn(Hdparm.COMMAND, [
            Hdparm.COMMAND_ARG_DRIVE_STATE,
            this.device
        ]);
        const matches: string[] = result.match(Hdparm.REGEX_DRIVE_STATE);
        // console.log(result, matches);
        if (matches === null) throw new Error(`not able to get drive state of ${this.device}`);
        return <DriveState>matches[1];
    }

    private setBusy(): void {
        this.checkIfBusy(false);
        this.isBusy = true;
    }

    private setUnbusy(): void {
        this.checkIfBusy(true);
        this.isBusy = false;
    }

    private checkIfBusy(shouldBeBusy: boolean): void {
        if (shouldBeBusy !== this.isBusy)
            throw new Error(
                shouldBeBusy ?
                    'is not busy' :
                    'is busy'
            );
    }

}
