import 'source-map-support/register';

import {PromiseUtil} from '@beautils/all';
import {Rsync, RsyncError} from './index';

class Example {

    async run(): Promise<void> {
        this.log('main', '------------------------- BEGIN ---------------------------');

        const rsync: Rsync = new Rsync({
            commandOrArgs: 'rsync -avz --bwlimit=1000 --progress ./source/ ./destination/',
            executableShell: '/bin/bash',
            cwd: '/home/albrecht/_temp/rsyncTest/'
        });
        this.log('main', 'command:', rsync.command);

        rsync.stdout.subscribe((data: Buffer) => {
            this.log('stdout', data.toString());
        });
        rsync.stderr.subscribe((data: Buffer) => {
            this.log('stderr', data.toString());
        });

        rsync.execute().catch(this.catcher.bind(this));
        await PromiseUtil.sleep(100);
        rsync.kill().catch(this.catcher.bind(this));
        await PromiseUtil.sleep(100);
        rsync.execute().catch(this.catcher.bind(this));
        rsync.kill().catch(this.catcher.bind(this));

        await PromiseUtil.sleep(5000);

        rsync.execute().catch(this.catcher.bind(this));
        await PromiseUtil.sleep(8000);
        rsync.kill().catch(this.catcher.bind(this));

        await PromiseUtil.immediately();
        this.log('main', '------------------------- END ---------------------------');
    }

    private catcher(err: RsyncError): void {
        console.log('!!!!!!!!!!!Error!!!!!!!!!!!!');
        console.log('message:', err.message);
        if (err instanceof RsyncError) {
            console.log('code:', err.code);
            console.log('reason:', err.reason);
        }
        console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    }

    private log(subject: string, ...args: any[]): void {
        console.log.apply(console.log, [`[${subject}]`].concat(args));
    }

}

new Example().run();
