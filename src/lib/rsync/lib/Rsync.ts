import path from 'path';
import {ChildProcess, spawn} from 'child_process';
import {Subject} from 'rxjs';
import {PromiseUtil} from '@beautils/all';
import {RsyncExitCode} from './RsyncExitCode';
import {RsyncError} from './RsyncError';
import {RsyncOptions} from './RsyncOptions';

export class Rsync {

    public static readonly COMMAND: string = 'rsync';
    private static readonly DEFAULT_OPTIONS: RsyncOptions = <RsyncOptions>{
        cwd: process.cwd(),
        env: process.env,
        executable: Rsync.COMMAND,
        executableShell: '/bin/sh',
        debug: false,
    };
    public readonly stdout: Subject<Buffer> = new Subject<Buffer>();
    public readonly stderr: Subject<Buffer> = new Subject<Buffer>();

    private errorsFromStderr: string[] = [];
    private readonly options: RsyncOptions;
    private childProcess: ChildProcess;

    private executionPromise: Promise<void>;
    private killPromise: Promise<void>;

    public constructor(options: RsyncOptions) {
        this.options = {
            ...Rsync.DEFAULT_OPTIONS,
            ...options,
            cwd: options.cwd ?
                path.resolve(options.cwd) :
                Rsync.DEFAULT_OPTIONS.cwd,
            env: options.env ?
                {...Rsync.DEFAULT_OPTIONS.env, ...options.env} :
                Rsync.DEFAULT_OPTIONS.env
        };
    }

    public get command(): string {
        const args: string[] = this.options.commandOrArgs
            .split(/[\ ]+/)
            .filter((arg: string) => arg.search(/^rsync$/i) === -1);
        return `${this.options.executable} ${args.join(' ')}`;
    }

    public async execute(): Promise<void> {
        // console.log('execute', this.isRunning, this.isDying);
        if (this.isDying) throw new RsyncError('rsync process currently dying');
        if (this.isRunning) throw new RsyncError('rsync process is already running');
        return this.executionPromise = this.doExecute();
    }

    public async kill(): Promise<void> {
        // console.log('kill', this.isRunning, this.isDying);
        return this.killPromise = this.doKill();
    }

    private doExecute(): Promise<void> {
        // console.log('doExecute');
        return new Promise<void>(
            (resolve: () => void, reject: (err: Error) => void) => {
                const isWin32: boolean = process.platform === "win32";

                this.childProcess = spawn(
                    isWin32 ? 'cmd.exe' : this.options.executableShell,
                    isWin32 ?
                        ['/s', '/c', `"${this.command}"`] :
                        ['-c', this.command],
                    {
                        stdio: 'pipe',
                        windowsVerbatimArguments: isWin32 || undefined,
                        cwd: this.options.cwd,
                        env: this.options.env,
                    }
                );
                this.childProcess.stdout.on('data', this.stdout.next.bind(this.stdout));
                this.childProcess.stderr.on('data', (data: Buffer) => {
                    this.errorsFromStderr.push(data.toString());
                    this.stderr.next(data);
                });
                this.childProcess.on('close', (exitCode: number) => {
                    if (exitCode === RsyncExitCode.SUCCESS) return this.resolveExecution(resolve);
                    return this.rejectExecution(reject, exitCode);
                });
            }
        );
    }

    private doKill(): Promise<void> {
        if (this.isRunning) {
            this.childProcess.kill('SIGTERM');
            return this.executionPromise;
        }
        if (this.isDying) return this.killPromise;
    }

    private async resolveExecution(origResolve: () => void): Promise<void> {
        origResolve();
        this.reset();
    }

    private async rejectExecution(origReject: (err: Error) => void, exitCode: number): Promise<void> {
        await PromiseUtil.immediately(); //await next tick for populating errors from stdout
        const msg: string = `rsync exited with code ${exitCode}`;
        origReject(new RsyncError(msg, exitCode, this.errorsFromStderr.join('')));
        this.reset();
    }

    private get isRunning(): boolean {
        return this.executionPromise !== undefined;
    }

    private get isDying(): boolean {
        return this.killPromise !== undefined;
    }

    private reset(): void {
        // console.log('reset()');
        this.errorsFromStderr = [];
        this.childProcess = undefined;
        this.executionPromise = undefined;
        this.killPromise = undefined;
    }

}
