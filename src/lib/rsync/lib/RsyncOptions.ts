export interface RsyncOptions {
    commandOrArgs: string;
    cwd?: string;
    executable?: string;
    executableShell?: string;
    env?: NodeJS.ProcessEnv;
    debug?: boolean;
}
