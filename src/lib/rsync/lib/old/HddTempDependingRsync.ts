import {HddTempDependingRsyncOptions} from './HddTempDependingRsyncOptions';

// https://unix.stackexchange.com/questions/11311/how-do-i-find-on-which-physical-device-a-folder-is-located
// https://linuxconfig.org/obtain-hard-drive-temperature-information-using-linux

export class HddTempDependingRsync  {

    private static readonly DEFAULT_OPTIONS: HddTempDependingRsyncOptions = <HddTempDependingRsyncOptions>{
    };

    // public constructor(options: HddTempDependingRsyncOptions) {
    //     super({
    //         ...HddTempDependingRsync.DEFAULT_OPTIONS,
    //         ...options
    //     });
    // }

    // //override
    // protected async setup(rsyncInstance: RsyncTypeFromLib): Promise<void> {
    //     console.log('setup start');
    //     rsyncInstance.set('--append-verify');
    //     await PromiseUtil.sleep(2000);
    //     console.log('setup end');
    // }
    //
    // //override
    // protected async execute(): Promise<boolean> {
    //     console.log('execute start');
    //     await PromiseUtil.sleep(3000);
    //     console.log('execute end');
    //     return true;
    // }

}
