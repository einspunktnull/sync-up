export class RsyncWrapperError extends Error {
    public readonly command: string;
    public readonly code: number;
    public readonly originalError: Error;

    constructor(message: string, origErr?: Error, code?: number, command?: string) {
        super(message);
        this.originalError = origErr;
        this.code = code;
        this.command = command;
    }
}
