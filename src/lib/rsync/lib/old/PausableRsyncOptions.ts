export interface PausableRsyncOptions {
    runDuration: string | number;
    pauseDuration: string | number;
}
