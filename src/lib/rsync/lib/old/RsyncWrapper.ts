import {Subject} from 'rxjs';
import {RsyncWrapperError} from './RsyncWrapperError';
import {RsyncWrapperOptions} from './RsyncWrapperOptions';
import {Rsync} from '../../index';

export class RsyncWrapper {

    public readonly errors: Subject<RsyncWrapperError> = new Subject<RsyncWrapperError>();
    public readonly statusLog: Subject<any> = new Subject<any>();
    public readonly stdout: Subject<Buffer> = new Subject<Buffer>();
    public readonly stderr: Subject<Buffer> = new Subject<Buffer>();

    private readonly options: RsyncWrapperOptions = <RsyncWrapperOptions>{};

    private _rsyncInstance: Rsync;


    public constructor(options: RsyncWrapperOptions) {
        this.options = {
            ...this.options,
            ...options
        };
    }

    // public get command(): string {
    //     return this.rsyncInstance.command();
    // }
    //
    // public async run(): Promise<boolean> {
    //     if (this.hasPendingExecutionPromise) throw this.addError(new RsyncWrapperError('already running!'));
    //     if (this.hasPendingKillPromise) throw this.addError(new RsyncWrapperError('is dying!'));
    //     return this.executeChildProcess();
    // }
    //
    // public async kill(): Promise<boolean> {
    //     if (this.hasPendingKillPromise) return this.killPromise;
    //     return this.killChildProcess();
    // }
    //
    // private async killChildProcess(): Promise<boolean> {
    //     if (this.hasPendingExecutionPromise) {
    //         this.childProcess.kill('SIGTERM');
    //         await this.executionPromise;
    //     }
    //     return false;
    // }
    //
    // private async executeChildProcess(): Promise<boolean> {
    //     return this.executionPromise = new Promise<boolean>((resolve, reject) => {
    //         this.childProcess = this.rsyncInstance.execute(
    //             (rsyncError: Error, code: number) => {
    //                 this.addStatus(
    //                     'onResyncEvent',
    //                     'err:', rsyncError,
    //                     'code:', code
    //                 );
    //                 if (isNil(code) || code === RsyncExitCode.RECEIVED_SIGUSR1_OR_SIGINT) resolve(false);
    //                 else if (code === RsyncExitCode.SUCCESS) resolve(true);
    //                 else reject(rsyncError);
    //                 this.reset();
    //             },
    //             this.stdout.next.bind(this.stdout),
    //             this.stderr.next.bind(this.stderr)
    //         );
    //     });
    // }
    //
    // private get rsyncInstance(): RsyncTypeFromLib {
    //     const hasRsyncInstance: boolean = !isNil(this._rsyncInstance);
    //     if (!hasRsyncInstance) this._rsyncInstance = this.createBasicRsyncCommand();
    //     return this._rsyncInstance;
    // }
    //
    // private set rsyncInstance(rsyncInstance: RsyncTypeFromLib) {
    //     this._rsyncInstance = rsyncInstance;
    // }
    //
    // private get hasPendingExecutionPromise(): boolean {
    //     return !isNil(this.executionPromise);
    // }
    //
    // private get hasPendingKillPromise(): boolean {
    //     return !isNil(this.killPromise);
    // }
    //
    // private createBasicRsyncCommand(): RsyncTypeFromLib {
    //     const rsyncInstance: RsyncTypeFromLib = new rsync();
    //     if (isString(this.options.cwd)) rsyncInstance.cwd(this.options.cwd);
    //
    //     const cmd:RsyncWrapperCommand = this.options.command;
    //     rsyncInstance.destination(cmd.destination);
    //     cmd.sources.forEach(rsyncInstance.source.bind(rsyncInstance));
    //     cmd.flags.forEach(rsyncInstance.flags.bind(rsyncInstance));
    //
    //
    //     // rsyncInstance.progress();
    //     // rsyncInstance.set('bwlimit', '1000');
    //     return rsyncInstance;
    // }
    //
    // private reset(): void {
    //     this.rsyncInstance = undefined;
    //     this.executionPromise = undefined;
    //     this.childProcess = undefined;
    // }
    //
    // private addStatus(...args: any[]): void {
    //     this.statusLog.next(args);
    // }
    //
    // private addError(error: RsyncWrapperError): RsyncWrapperError {
    //     this.errors.next(error);
    //     return error;
    // }

}
