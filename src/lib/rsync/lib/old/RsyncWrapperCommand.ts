export interface RsyncWrapperCommand {
    sources: string[];
    destination: string;
    flags?: string[];
    option?:string[][];
}
