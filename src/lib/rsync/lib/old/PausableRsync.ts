import {PausableRsyncOptions} from './PausableRsyncOptions';

export class PausableRsync {

    private static readonly DEFAULT_OPTIONS: PausableRsyncOptions = <PausableRsyncOptions>{
        pauseDuration: '10s',
        runDuration: '10s',
    };

    // public constructor(options: PausableRsyncOptions) {
    //     super({
    //         ...PausableRsync.DEFAULT_OPTIONS,
    //         ...options
    //     });
    // }
    //
    // //override
    // protected async setup(rsyncInstance: RsyncTypeFromLib): Promise<void> {
    //     rsyncInstance.set('--append-verify');
    // }
    //
    // //override
    // protected async execute(): Promise<boolean> {
    //     const runDuration: number = DateAndTimeUtil.toMilliseconds(this.options.runDuration);
    //     const pauseDuration: number = DateAndTimeUtil.toMilliseconds(this.options.pauseDuration);
    //     while (true) {
    //         this.addStatus(`run rsync for ${runDuration} milliseconds`);
    //         const successfullFinished: boolean = await this.runAndKillIfTimeout(runDuration);
    //         this.addStatus('ran rsync. finished: ', successfullFinished);
    //         if (successfullFinished) return successfullFinished;
    //         await PromiseUtil.sleep(pauseDuration);
    //     }
    // }
    //
    // private async runAndKillIfTimeout(timeout: number): Promise<boolean> {
    //     let timeoutObj: NodeJS.Timeout = setTimeout(async () => {
    //         await this.kill();
    //     }, timeout);
    //     const successfullFinished: boolean = await super.execute();
    //     clearTimeout(timeoutObj);
    //     return successfullFinished;
    // }

}
