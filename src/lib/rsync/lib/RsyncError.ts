export class RsyncError extends Error {
    private readonly _code: number;
    private readonly _reason: string;

    public get code(): number {
        return this._code;
    }

    public get reason(): string {
        return this._reason;
    }

    public constructor(message: string, code?: number, reason?: string) {
        super(message);
        this._code = code;
        this._reason = reason;
    }
}
