export type GetSet<T> = {
    get: () => T
    set: (value: T) => void
};
export type RawData<T> = {
    ___rawData: {
        parent: PropertyFull<any>;
        value: T;
        children?: Record<string, PropertyFull<any>>;
        key: string;
        path: string;
    }
}
export type PropertyLight<T> = T & GetSet<T>;
export type PropertyFull<T> = PropertyLight<T> & RawData<T>;
export type PropertyOnly<T> = GetSet<T> & RawData<T>;

export type Property<T> = PropertyLight<T>;
