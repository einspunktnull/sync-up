import lodash from 'lodash';
import {expect} from '@beautils/all';
import {Property, PropertyFull, PropertyOnly} from './types';


export class Properties<T> {

    private static KEY_ROOT: string = 'root';

    private readonly _origValue: T;
    private readonly _root: PropertyFull<T>;

    public constructor(value: T) {
        this._origValue = value;
        const copy: T = lodash.cloneDeep(value);
        this._root = this.toPropertyObject(copy, Properties.KEY_ROOT);
    }

    public get root(): Property<T> {
        return this._root;
    }

    private get<T>(prop: PropertyOnly<T>): T {
        return prop.___rawData.value;
    }

    private set<T>(prop: PropertyOnly<T>, value: T): void {
        expect(prop.___rawData.parent).isNotNil(new Error('root. no parent'));
        const propPath: string = prop.___rawData.path;
        const parentPath: string = prop.___rawData.parent?.___rawData.path;
        console.log('Properties.get', prop, propPath, parentPath, parentPath);
        console.log('Properties.set', prop, value);
        prop.___rawData.value = value;
    }

    private toPropertyObject<T>(
        value: T,
        key: string,
        parentProp: PropertyFull<any> = null
    ): PropertyFull<T> {
        const keyStr: string = lodash.isNumber(key) ? `[${key}]` : key;
        const path: string = parentProp ? parentProp.___rawData.path + '.' + keyStr : keyStr;
        const prop: PropertyFull<T> = <PropertyFull<T>>{
            ___rawData: {
                value: value,
                parent: parentProp,
                key: key,
                path: path,
                children: undefined,
            },
            get: () => this.get(prop),
            set: (value: T) => this.set(prop, value)
        };
        const isArray: boolean = lodash.isArray(value);
        const isObject: boolean = lodash.isPlainObject(value);
        const isArrayOrObject: boolean = isArray || isObject;
        if (!isArrayOrObject) return prop;
        const children: Record<string, PropertyFull<any>> = {};
        for (const childKey in value) {
            const childValue: any = value[childKey];
            const child: PropertyFull<any> = this.toPropertyObject(childValue, childKey, prop);
            expect(prop.hasOwnProperty(childKey))
                .isFalse(`could not use propname for: "${childKey}"`);
            children[childKey] = child;
            Object.defineProperty(prop, childKey, {
                get: () => child
            });
        }
        prop.___rawData.children = children;
        return prop;
    }

}


