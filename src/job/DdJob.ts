import {Job} from '../core/job/Job';
import {
    DdConfig,
    DdPathConfig,
    FindDiskConfig,
    FindDiskImageConfig,
    FindPartitionConfig
} from '../core/config/SyncUpConfig';
import {isEmpty, isString} from 'lodash';
import {DiskInfo, DiskPartitionInfo} from '../service/systemInfo/types';
import {Inject} from 'typescript-ioc';
import {FindService} from '../service/find/FindService';
import {MountService} from '../service/mount/MountService';
import {v1 as uuidV1} from 'uuid';
import path from 'path';
import {DiskdumpService} from '../service/diskdump/DiskdumpService';

export class DdJob extends Job {

    @Inject
    private readonly findService: FindService;

    @Inject
    private readonly mountService: MountService;

    protected async exec(): Promise<void> {
        const config: DdConfig = this.getConfig<DdConfig>();
        this.logService.info(`Find source path for "${config.source.name}"`);
        const sourcePath: string = await this.findPath(config.source);
        this.logService.info(`Found source path "${sourcePath}"`);
        this.logService.info(`Find source disk path for "${config.source.name}"`);
        const sourceDiskPath: string = await this.findService.findDiskDevicePath(sourcePath);
        this.logService.info(`Found source disk path "${sourceDiskPath}"`);

        this.logService.info(`Find destination path for "${config.destination.name}"`);
        const destinationPath: string = await this.findPath(config.destination);
        this.logService.info(`Found destination path "${destinationPath}"`);
        this.logService.info(`Find destination disk path for "${config.destination.name}"`);
        const destinationDiskPath: string = await this.findService.findDiskDevicePath(destinationPath);
        this.logService.info(`Found destination disk path "${destinationDiskPath}"`);

        const diskdumpService: DiskdumpService = new DiskdumpService({
                path: sourcePath,
                diskPath: sourceDiskPath,
                diskProtection: config.source.diskProtection
            }, {
                path: destinationPath,
                diskPath: destinationDiskPath,
                diskProtection: config.destination.diskProtection
            }
        );
        await diskdumpService.run();
    }

    protected async kill(): Promise<void> {
    }

    private async findPath(pathConfig: DdPathConfig): Promise<string> {
        const pathOrFindConfig: string | FindDiskConfig | FindPartitionConfig = pathConfig.path;
        if (isString(pathOrFindConfig)) return pathOrFindConfig;
        switch (pathConfig.type) {
            case "disk":
                return this.findDiskPath(pathOrFindConfig as FindDiskConfig);
            case "partition":
                return this.findPartitionPath(pathOrFindConfig as FindPartitionConfig);
            case "image":
                return this.findImagePath(pathOrFindConfig as FindDiskImageConfig);
        }
        throw new Error('unknown path config type: ' + pathConfig.type);
    }

    private async findDiskPath(findDiskConfig: FindDiskConfig): Promise<string> {
        const disk: DiskInfo = await this.findService.findDiskByConfig(findDiskConfig);
        if (disk === undefined) throw new Error('disk not found: ' + findDiskConfig);
        return disk.path;
    }

    private async findPartitionPath(findPartitionConfig: FindPartitionConfig): Promise<string> {
        const partInfo: DiskPartitionInfo = await this.findService.findPartitionByConfig(findPartitionConfig);
        if (partInfo === undefined) throw new Error('partition not found: ' + findPartitionConfig);
        return partInfo.path;
    }

    private async findImagePath(findDiskImageConfig: FindDiskImageConfig): Promise<string> {
        const partitionPath: string = await this.findPartitionPath(findDiskImageConfig as FindPartitionConfig);
        const mountPoint: string = await this.mountService.mount(partitionPath);
        let imageName: string = findDiskImageConfig.filename;
        if (!isString(imageName) || isEmpty(imageName)) imageName = uuidV1() + '.img';
        return mountPoint + path.sep + imageName;
    }

}
