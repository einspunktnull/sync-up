import 'source-map-support/register';
import {Command, CommandConstructor} from 'commander';
import {ExitCode} from './core/ExitCode';
import {MainProcess} from './process/MainProcess';
import {PackageJsonUtil, SingleInstanceError, SingleInstanceErrorCode} from '@beautils/all';
import {run} from './run';
// import {DependencyUtil} from './lib/util/DependencyUtil';
// import {Dependencies} from './core/Constants';
// import {ProcessUtil2} from './lib/util/ProcessUtil2';

class RunApp {

    public static async run(): Promise<void> {
        return new RunApp().run();
    }

    private async run(): Promise<void> {
        const configFile: string = await this.init();
        await this.start(configFile);
    }

    private async init(): Promise<string> {

        // try {
        //     await DependencyUtil.checkDependencies(Dependencies);
        // } catch (e) {
        //     SyncUpApp.kill(e, ExitCode.MISSING_DEPENDENCIES);
        // }
        //
        // try {
        //     await ProcessUtil2.expectSuperuser();
        // } catch (e) {
        //     SyncUpApp.kill(e, ExitCode.NO_SUPERUSER);
        // }

        const version: string = await PackageJsonUtil.getVersion(__dirname);
        const command: InstanceType<CommandConstructor> = new Command()
            .version(
                version,
                '-v, --version',
                'output the current version'
            )
            .requiredOption(
                '-c, --config <config>',
                'multiple YAML config files',
                'config.yml'
            )
            .parse(process.argv);

        return command.config;
    }

    private async start(configFile: string): Promise<void> {
        try {
            await MainProcess.init(configFile);
        } catch (e) {
            if ((<SingleInstanceError>e).code === SingleInstanceErrorCode.INSTANCE_ALREADY_RUNNING)
                RunApp.kill(e, ExitCode.INSTANCE_ALREADY_RUNNING);
            else throw e;
        }
    }

    private static kill(error: Error, exitCode: number): Error {
        console.error(`Error: ${error.message ?? error}`);
        process.exit(exitCode);
    }
}

run(RunApp.run.bind(RunApp));


