import 'source-map-support/register';

import {JobProcess} from './process/JobProcess';
import {expect} from '@beautils/all';
import {run} from './run';

run(async () => {
    const argv: string[] = process.argv;
    const mainProcessId: string = argv[2];
    const thisProcessId: string = argv[3];
    expect(mainProcessId)
        .isNotUndefined('undefined mainProcessId (process.argv[2])');
    expect(thisProcessId)
        .isNotUndefined('undefined processName (process.argv[3])');
    switch (thisProcessId) {
        case JobProcess.name:
            await JobProcess.init(mainProcessId);
            break;
        default:
            throw new Error('unknown job');
    }
});
