import {ProcessName} from '../core/constants';
import {ChildProcess} from '../core/process/ChildProcess';
import {DeviceConfig, SyncUpConfig} from '../core/config/SyncUpConfig';
import {FindService} from '../service/find/FindService';
import {Inject} from 'typescript-ioc';

export class JobProcess extends ChildProcess {

    public static async init(mainProcessId: string): Promise<void> {
        await new JobProcess(mainProcessId).initAsync();
    }

    @Inject
    private readonly findService:FindService;

    private deviceConfigs: DeviceConfig[];

    public constructor(mainProcessId: string) {
        super(mainProcessId, ProcessName.CHILD_JOBS, true);
    }

    protected async beforeStart(): Promise<void> {
        const config: SyncUpConfig = await this.configService.get();
        this.logService.log('config',config);
        this.deviceConfigs = await this.findService.findDeviceConfigs();
        this.logService.log('this.deviceConfigs',this.deviceConfigs);
    }

    protected async start(): Promise<void> {
        // console.log('JobProcess.start');
        // try {
        //     await this.jobService.execute(this.deviceConfigs);
        //     this.logService.info('All jobs are done. Undo things which has to be undone.');
        //     await this.undoService.execute();
        // } catch (e) {
        //     this.errorService.addErrorAndKillApp(e, ExitCode.UNKNOWN_ERROR);
        // }
    }

    protected async stop(): Promise<void> {
        // console.log('JobProcess.stop');
    }

}
