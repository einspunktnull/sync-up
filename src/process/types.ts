import {ChildProcess} from '../core/process/ChildProcess';

export type ProcessClass = { new(mainProcessId: string): ChildProcess };
