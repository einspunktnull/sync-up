import {Inject} from 'typescript-ioc';
import mkdirp from 'mkdirp';
import path from 'path';
import {SystemSummary} from '../service/systemInfo/types';
import {SystemInfoService} from '../service/systemInfo/SystemInfoService';
import {MotherProcess} from '../core/process/MotherProcess';
import {ProcessName} from '../core/constants';
import {FindFilesUtil, NodeCliUtil, NodeInspectData, NpmUtil, PromiseUtil} from '@beautils/all';
import child_process from 'child_process';
import {SyncUpConfig} from '../core/config/SyncUpConfig';
import {JobProcess} from './JobProcess';
import {ProcessClass} from './types';

export class MainProcess extends MotherProcess {

    public static async init(configFile: string): Promise<void> {
        await new MainProcess().initAsync(configFile);
    }

    @Inject
    private readonly hardwareInfoService: SystemInfoService;

    private constructor() {
        super(ProcessName.MAIN, true);
    }

    protected async beforeStart(): Promise<void> {
        const config: SyncUpConfig = await this.configService.get();
        this.logService.log('config', config);
        await mkdirp(path.resolve(config.app.dataDir));
        if (config.debugging?.enabled) {
            const systemSummary: SystemSummary = await this.hardwareInfoService.getSummary();
            this.logService.debug('systemSummary', systemSummary);
        }
    }

    protected async start(): Promise<void> {
        // console.log('MainProcess.start');
        await this.runChildProcesses([JobProcess]);
    }

    protected async stop(): Promise<void> {
        // console.log('MainProcess.stop');
    }

    protected async runChildProcesses(processClasses: ProcessClass[]): Promise<void> {
        // console.log('MainProcess.runChildProcesses', processClasses);
        const inspect: NodeInspectData = NodeCliUtil.nodeInspect;
        await PromiseUtil.allForEach(processClasses, this.runChildProcess.bind(this, inspect));
    }

    protected async runChildProcess(
        inspect: NodeInspectData,
        processClass: ProcessClass,
        index: number
    ): Promise<void> {
        // console.log('MainProcess.runChildProcess', processClass, typeof index);
        const className: string = processClass.name;
        const thisPackageDir: string = await NpmUtil.getPackagePath(__dirname);
        const filePath: string = await FindFilesUtil.findOne('index-child-process.js', thisPackageDir);
        // console.log('filePath!!!', filePath);
        const args: string[] = [this.processInfoService.id, className];
        const execArgv: string[] = inspect ? [
            `${inspect.option}=${inspect.host}:${inspect.port + (index + 1)}`
        ] : [];
        child_process.fork(filePath, args, {execArgv: execArgv});
    }

}
