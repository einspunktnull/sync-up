import {Inject} from 'typescript-ioc';
import {IpcService} from '../../service/ipc/IpcService';
import {ConfigService} from '../../service/config/ConfigService';
import {expect, SingleInstance} from '@beautils/all';
import {ProcessInfoService} from '../../service/processInfo/ProcessInfoService';
import {Initializable} from '../../lib/Initializable';
import {LogService} from '../../service/log/LogService';

export abstract class Process extends Initializable<void> {

    @Inject
    protected readonly processInfoService: ProcessInfoService;

    @Inject
    protected readonly configService: ConfigService;

    @Inject
    protected readonly logService: LogService;

    @Inject
    private readonly ipcService: IpcService;

    private readonly motherProcessId: string;
    private readonly idPrefixOrId: string;
    private readonly isSingleInstance: boolean;

    private singleInst: SingleInstance;

    protected constructor(motherProcessId: string, idPrefixOrId: string, isSingleInstance: boolean = false) {
        super();
        expect(idPrefixOrId).isNotUndefined();
        expect(motherProcessId).isNotUndefined();
        this.motherProcessId = motherProcessId;
        this.idPrefixOrId = idPrefixOrId;
        this.isSingleInstance = isSingleInstance;
    }

    protected abstract beforeStart(): Promise<void>;

    protected abstract start(): Promise<void>;

    protected abstract stop(): Promise<void>;

    protected async initialize(configFile?: string): Promise<void> {
        // console.log('Process.initialize', configFile);
        this.processInfoService.initSync(
            this.motherProcessId,
            this.idPrefixOrId,
            this.isSingleInstance
        );

        if (this.isSingleInstance) {
            const singleInstId: string = this.idPrefixOrId + '-single-instance';
            this.singleInst = await SingleInstance.lock(singleInstId);
        }

        await this.ipcService.initAsync();
        await this.configService.initAsync(configFile);
        await this.logService.initAsync();

        await this.beforeStart();
        await this.start();
        await this.stop();

        if (this.isSingleInstance) {
            await this.singleInst.unlock();
        }
    }

}
