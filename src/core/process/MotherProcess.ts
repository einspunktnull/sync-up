import {Process} from './Process';

export abstract class MotherProcess extends Process {
    protected constructor(idPrefixOrId: string, isSingleInstance: boolean = false) {
        super(idPrefixOrId, idPrefixOrId, isSingleInstance);
    }
}
