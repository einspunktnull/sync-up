import {Process} from './Process';

export abstract class ChildProcess extends Process {

    protected constructor(motherProcessId: string, idPrefixOrId: string, singleInstance: boolean = false) {
        super(motherProcessId, idPrefixOrId, singleInstance);
    }
}
