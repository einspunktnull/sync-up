import {Diskdump} from '../lib/diskdump/Diskdump';
import {Rsync} from '../lib/rsync';
import {Blkid} from '../lib/blkid/Blkid';
import {Hddtemp} from '../lib/hddtemp/Hddtemp';
import {Mount} from '../lib/mount/Mount';
import {Hdparm} from '../lib/hdparm/Hdparm';

export const LogDir: string = 'logs';
export const StorageDir: string = 'storage';
export const MountDir: string = 'mount';

export enum JobType {
    dd = 'dd',
    rsync = 'rsync',
}

export enum DdStorageType {
    Partition = 'partition',
    Disk = 'disk',
    Image = 'image'
}

export enum JobState {
    PENDING = 'PENDING',
    RUNNING = 'RUNNING',
    KILLING = 'KILLING',
    KILLED = 'KILLED',
    FINISHED = 'FINISHED',
}


export enum IpcEvent {
    GET_CONFIG = "GET_CONFIG",
    SET_CONFIG = "SET_CONFIG"
}

export enum ProcessName {
    MAIN = 'sync-up-mother-main',
    CHILD_JOBS = 'sync-up-child-jobs',
    CHILD_WEB = 'sync-up-child-web',
}

export enum LogLevel {
    fatal = 'fatal',
    error = 'error',
    warn = 'warn',
    info = 'info',
    debug = 'debug',
    trace = 'trace',
}

export const Dependencies: string[] = [
    Diskdump.COMMAND,
    Rsync.COMMAND,
    Blkid.COMMAND,
    Hddtemp.COMMAND,
    Mount.COMMAND_MOUNT,
    Mount.COMMAND_UMOUNT,
    'tee',
    'realpath',
    'df',
    'grep',
    'cut',
    'lsblk',
    Hdparm.COMMAND
];

