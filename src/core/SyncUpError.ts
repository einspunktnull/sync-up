export class SyncUpError extends Error {
    public constructor(
        message: string,
        public readonly origError?: any,
        public readonly args?: any[],
    ) {
        super(message);
    }
}
