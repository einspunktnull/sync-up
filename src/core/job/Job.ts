import {JobConfig} from '../config/SyncUpConfig';
import {Inject} from 'typescript-ioc';
import {v1 as uuidV1} from 'uuid';
import {JobState} from "../constants";
import {LogService} from '../../service/log/LogService';

export abstract class Job {

    @Inject
    protected readonly logService: LogService;

    private readonly deviceId: string;
    private readonly jobConfig: JobConfig;

    private _state: JobState = JobState.PENDING;
    private readonly _uuid: string = uuidV1();

    public toString(): string {
        const cfg: JobConfig = this.jobConfig;
        return `Job [ name: ${cfg.name}, type: ${cfg.type}, uuid: ${this.uuid} ]`;
    }

    public getConfig<TJobConfig extends JobConfig>(): TJobConfig {
        return this.jobConfig as TJobConfig;
    }

    public get state(): JobState {
        return this._state;
    }

    public get uuid(): string {
        return this._uuid;
    }

    protected setState(state: JobState): void {
        this._state = state;
    }

    public constructor(deviceId: string, jobConfig: JobConfig) {
        this.deviceId = deviceId;
        this.jobConfig = jobConfig;
        this.setState(JobState.PENDING);
    }

    public async run(): Promise<void> {
        if (this.state !== JobState.PENDING) throw new Error('not pending');
        this.logService.info('Start Job', this.toString());
        this.setState(JobState.RUNNING);
        await this.exec();
        this.setState(JobState.FINISHED);
    }

    public async stop(): Promise<void> {
        if (this.state !== JobState.RUNNING) throw new Error('not running');
        this.logService.info('Stop Job', this.toString());
        this.setState(JobState.KILLING);
        await this.kill();
        this.setState(JobState.KILLED);
    }

    protected abstract exec(): Promise<void>;

    protected abstract kill(): Promise<void>;

}
