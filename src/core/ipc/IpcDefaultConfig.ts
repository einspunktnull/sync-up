import {Config} from './types';

export const IpcDefaultConfig: Config = {
    retry: 1500,
    maxRetries: 1,
    silent: true
};
