import {IpcCore} from './IpcCore';
import {Initializable} from '../../lib/Initializable';
import {OrPromise, Resolve} from 'beautiful-types';
import {CreateAnswerValueFct, Dto, Event} from './types';

export class Ipc extends Initializable<Ipc> {

    public static async create(serverId: string, id: string): Promise<Ipc> {
        return await new Ipc(serverId, id).initialize();
    }

    private core: IpcCore;

    protected constructor(serverId: string, id: string) {
        super();
        this.core = new IpcCore(serverId, id);
    }

    public get id(): string {
        return this.core.id;
    }

    public get serverId(): string {
        return this.core.serverId;
    }

    public get isServer(): boolean {
        return this.core.isServer;
    }

    protected async initialize(): Promise<this> {
        await this.core.initAsync();
        return this;
    }

    public answer<Tin, Tout>(event: Event, answerValueFct: CreateAnswerValueFct<Tin, Tout>): void {
        // console.log('IpcService.answer()', this.id, topic, data);
        this.core.on(
            Helper.asQuestionEvent(event),
            async (questionDto: Dto<Tin>) => {
                const answerFctReturnValue: OrPromise<Tout> = answerValueFct(
                    questionDto.sender,
                    questionDto.data
                );
                const answerData: Tout = answerFctReturnValue instanceof Promise ?
                    await answerFctReturnValue :
                    answerFctReturnValue;
                this.core.send(
                    Helper.asAwnserEvent(event),
                    answerData
                );
            }
        );
    }

    public ask<Tout, Tin>(event: Event, data?: Tout): Promise<Tin> {
        // console.log('IpcService.ask()', this.id, topic, data);
        return new Promise<Tin>((resolve: Resolve<Tin>) => {
            this.core.once(Helper.asAwnserEvent(event),
                (data: Dto<Tin>) => resolve(data.data)
            );
            this.core.send(Helper.asQuestionEvent(event), data);
        });
    }

    // public subscribe<T>(topic: string, subscriber: Subscriber<T>): void {
    //     this.ipc.on(topic, (data: Data<T>) => {
    //         subscriber(data.data);
    //     });
    // }
    //
    // public send<T>(topic: string, data?: T): void {
    //     this.ipc.send(topic, data);
    // }

}

class Helper {

    public static asQuestionEvent(topic: Event): Event {
        return `QUESTION_${topic}`;
    }

    public static asAwnserEvent(topic: Event): Event {
        return `ANSWER_${topic}`;
    }
}
