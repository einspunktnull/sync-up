import nodeIpc from 'node-ipc';
import {Callback, OrPromise} from 'beautiful-types';

export type Config = Partial<typeof nodeIpc.config>;
export type Server = Partial<typeof nodeIpc.server>;
export type Client = {
    on(event: Event, callback: Callback): void;
    emit(event: Event, value: any): Client;
    off(event: Event, handler: any): Client;
};
export type ClientOrServer = Client | Server;
export type Dto<T> = { sender: string; data?: T };
export type EventCallback<T> = (data: Dto<T>) => void;
export type Event = string;
export type SubscriptionId = string;
export type Subscription<T> = { id: SubscriptionId; event: Event; callback: EventCallback<T> };
export type CreateAnswerValueFct<Tin, Tout> = (sender: string, data: Tin) => OrPromise<Tout>;
// export type Subscriber<T> = (data: T) => void;
// export type AwnserData<T> = IpcData<T>;
// export type QuestionData<T> = IpcData<T>;
