import nodeIpc from 'node-ipc';
import {v1 as uuidV1} from 'uuid';
import {Client, ClientOrServer, Dto, Event, EventCallback, Server, Subscription, SubscriptionId} from './types';
import {IpcDefaultConfig} from './IpcDefaultConfig';
import {Socket} from 'net';
import {Initializable} from '../../lib/Initializable';
import {Reject, Resolve} from 'beautiful-types';

export class IpcCore extends Initializable<void> {

    protected readonly _serverId: string;
    protected startResolve: Resolve<void>;
    protected startReject: Reject;
    protected _ipcInstance: ClientOrServer;

    private readonly _id: string;
    private readonly subs: Map<SubscriptionId, Subscription<any>> = new Map<SubscriptionId, Subscription<any>>();
    private readonly sockets: Socket[] = [];

    public constructor(serverId: string, id: string) {
        super();
        this._serverId = serverId;
        this._id = id;
        nodeIpc.config.id = this.id;
        for (const key in IpcDefaultConfig) {
            nodeIpc.config[key] = IpcDefaultConfig[key];
        }
    }

    protected async initialize(): Promise<void> {
        return new Promise<void>(async (resolve: Resolve<void>, reject: Reject) => {
            this.startResolve = resolve;
            this.startReject = reject;
            this.isServer ? this.startServer() : this.startClient();
        });
    }

    public get id(): string {
        return this._id;
    }

    public get serverId(): string {
        return this._serverId;
    }

    public get isServer(): boolean {
        return this.serverId === this.id;
    }

    public send<T>(event: Event, data: T): void {
        // console.log('CoreIpc.send()', this.id, topic, data);
        const ipcData: Dto<T> = {
            sender: this.id,
            data: data
        };
        if (this.isServer) {
            this.sockets.forEach((sock: Socket) => {
                this.server.emit(sock, event, ipcData);
            });
        } else {
            this.client.emit(event, ipcData);
        }
    }

    public once<T>(event: Event, onCallback: EventCallback<T>): void {
        // console.log('IpcCore.once()', this.id, topic);
        const subId: string = this.on(event, (data: Dto<T>) => {
            onCallback(data);
            this.off(subId);
        });
    }

    public on<T>(event: Event, onCallback: EventCallback<T>): string {
        // console.log('IpcCore.on()', this.id, event);
        const id: string = uuidV1();
        const onEvent: EventCallback<T> = (data: Dto<T>) => {
            // console.log('IpcCore.onEvent()', this.id, data);
            onCallback(data);
        };
        this.subs.set(id, {
            id: id,
            callback: onEvent,
            event: event
        });
        this._ipcInstance.on(event, onEvent);
        return id;
    }

    public off<T>(subId: SubscriptionId): void {
        // console.log('IpcCore.off()', this.id, subId);
        const sub: Subscription<T> = this.subs.get(subId);
        if (sub) {
            this.subs.delete(sub.id);
            this._ipcInstance.off(sub.event, sub.callback);
        }
    }

    protected get clientOrServer(): ClientOrServer {
        return this._ipcInstance;
    }

    protected get client(): Client | undefined {
        return this.isServer ? undefined : <Client>this.clientOrServer;
    }

    protected get server(): Server | undefined {
        return this.isServer ? <Server>this.clientOrServer : undefined;
    }

    protected set clientOrServer(clientOrServer: ClientOrServer) {
        this._ipcInstance = clientOrServer;
    }

    protected set server(server: Server) {
        this.clientOrServer = server;
    }

    protected set client(client: Client) {
        this.clientOrServer = client;
    }

    private startClient(): void {
        nodeIpc.connectTo(this._serverId, () => {
            this.client = nodeIpc.of[this._serverId];
            this.registerEventListeners();
        });
    }

    private startServer(): void {
        nodeIpc.serve(() => {
            if (this.startResolve) {
                this.startResolve();
                this.startResolve = undefined;
            }
        });
        this.server = nodeIpc.server;
        this.registerEventListeners();
        this.server.start();
    }

    private registerEventListeners(): void {
        this.clientOrServer.on("error", this.onError.bind(this));
        this.clientOrServer.on("connect", this.onConnect.bind(this));
        this.clientOrServer.on("disconnect", this.onDisconnect.bind(this));
        this.clientOrServer.on("data", this.onData.bind(this));
    }

    private onConnect(socket: Socket): void {
        if (socket) this.sockets.push(socket);
        // console.log(this.sockets);
        if (this.startResolve) {
            this.startResolve();
            this.startResolve = undefined;
        }
    }

    private onError(err: Error): void {
        // console.log('IpcCore.onError', this.id, err);
        if (this.startReject) {
            this.startReject(err);
            this.startReject = undefined;
        }
    }

    private onDisconnect(): void {
        console.log('IpcCore.onDisconnect', this.id);
    }

    private onData(data: any): void {
        console.log('IpcCore.onData()', this.id, data);
    }

}

