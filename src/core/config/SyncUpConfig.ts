import {DdStorageType, JobType, LogLevel} from '../constants';
import {Property} from '../../lib/properties/types';

export interface SyncUpConfig {
    app: Property<AppProcessConfig>;
    logging: Property<LoggingConfig>;
    devices: Property<Property<DeviceConfig>[]>;
    debugging?: Property<DebuggingConfig>;
}

export interface AppProcessConfig {
    dataDir: Property<string>;
    multipleInstances?: Property<boolean>;
}

export interface DebuggingConfig {
    enabled: Property<boolean>;
    logging?: Property<DebuggingLoggingConfig>;
    features?: Property<DebuggingFeaturesConfig>;
}

export interface DebuggingLoggingConfig {
    exclude: Property<string[]>;
    showFileAndLineNum: Property<boolean>;
}

export interface DebuggingFeaturesConfig {
    allowNoSuperuser: Property<boolean>;
}

export interface LoggingConfig {
    logger: Property<LoggerBaseConfig>;
    console: Property<LoggerBaseConfig>;
}

export interface LoggerBaseConfig {
    level: Property<LogLevel>;
}

export interface DeviceConfig {
    name: Property<string>;
    id: Property<string>;
    hardware: Property<HardwareConfig>;
    jobs: Property<Property<JobConfig>[]>;
    enabled?: Property<boolean>;
}

export interface HardwareConfig {
    mainboard: Property<string>;
    cpu: Property<string>;
    graphicsAdapters?: Property<Property<string>[]>;
}

export interface JobConfig {
    name: Property<string>;
    type: Property<JobType>;
    programArgs: Property<any | any[]>;
    description?: Property<string>;
    enabled?: Property<boolean>;
}

export interface RsyncConfig extends JobConfig {
    mountPoint: Property<string>;
}

export interface DdConfig extends JobConfig {
    source: Property<DdPathConfig>;
    destination: Property<DdPathConfig>;
}

export interface DdPathConfig {
    name: Property<string>;
    type: Property<DdStorageType>;
    path: Property<Property<string> |
        Property<FindDiskConfig> |
        Property<FindPartitionConfig> |
        Property<FindDiskImageConfig>>;
    diskProtection?: Property<DiskProtectionConfig>;
    description?: Property<string>;
}

export interface DiskProtectionConfig {
    minTemp: Property<number>;
    maxTemp: Property<number>;
    standBy?: Property<boolean>;
    interval?: Property<number | string>;
}

export interface DiskDetectConfig {
    waitForDisk?: Property<boolean>;
    scanScsi?: Property<boolean>;
}

export interface FindDiskConfig extends DiskDetectConfig {
    serial?: Property<string>;
    model?: Property<string>;
    vendor?: Property<string>;
    size?: Property<number | string>;
}

export interface FindPartitionConfig extends DiskDetectConfig {
    uuid?: Property<string>;
    partUuid?: Property<string>;
    label?: Property<string>;
    partLabel?: Property<string>;
    type?: Property<string>;
    blockSize?: Property<number | string>;
}

export interface FindDiskImageConfig extends FindPartitionConfig {
    filename?: Property<string>;
}

