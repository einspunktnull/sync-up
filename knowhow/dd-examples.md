#### Backup/Image
Make sure the `if` value is correct! If doing this over SSH then open a TMUX session first... or else...
```
dd if=/dev/YOUR-DEVICE conv=sync,noerror bs=64K | gzip -c  > /home/portaj/macbook.img.gz
```
NOTE: You might not want to compress the image. It just means you have to uncompress it later to mount it. If you're planning to access the data soon, or frequently, don't compress it.

##### Saving a copy of the drive geometry
Save it in the same directory as the compressed image so later on if you decide you want to mount or extract data from the image you can see the partition structure without having to decompress the whole image. There might be some other ways to mount a compressed image.
```
fdisk -l /dev/YOUR-DEVICE > /home/portaj/macbook_fdisk.info
```

#### Restore/Forcibly write your backup to another device
Make damn sure the `of` value is correct or you might obliterate your data.
Leave out `sync` for faster writes or when writing to flash memory - you can leave out the `noerror` in many cases as well.
```
gunzip -c /home/portaj/macbook.img.gz | dd of=/dev/YOUR-DEVICE-DONT-EFF-THIS-UP conv=sync,noerror bs=64
```

#### Mounting the image
##### Windows
This program works well: http://www.osforensics.com/tools/mount-disk-images.html

##### Linux
You need to determine the block offset of the parition you want to mount, then you can mount it. For example, we need to scoot 63 sectors ahead, and each sector is 512 bytes long, or however many you specified in the original image command `bs=64k`, we need to use an offset of 32,256 bytes
```
fdisk -l /home/portaj/macbook.img
                    Device Boot      Start         End      Blocks   Id  System
macbook.img                *          63    33640109    16820023+  83  Linux
```

But, if the image is compressed, you can't do that. You need to uncompress it first.
```
gzip -dc /home/portaj/macbook.img.gz | dd of=/home/portaj/macbook.img
```

Then mount the image at the correct block - you know, the block index that you calculated from the above commands. Go read it...
```
mount -o ro,loop,offset=32256 /home/portaj/macbook.img /mnt/loop
mount | grep /home/portaj/macbook.img

/root/macbook.img on /mnt/loop type ext3 (ro,loop=/dev/loop1,offset=32256)
```

#### References

Imaging info grabbed from: http://www.linuxweblog.com/dd-image

Mounting info grabbed from: https://major.io/2010/12/14/mounting-a-raw-partition-file-made-with-dd-or-dd_rescue-in-linux

Ideas on making images smaller by writing zeros to unneeded space, see the accepted answer: http://unix.stackexchange.com/questions/31669/is-it-possible-to-mount-a-gzip-compressed-dd-image-on-the-fly