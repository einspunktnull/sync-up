# sync-up
sync backup and archive tools

## Dependencies

### rsync
- `sudo rsync --whole-file -rlptgoDhvcAXHE --progress --noatime --stats --delete --partial --partial-dir=backup_partial /home/bla/ /media/something/bla/`

### dd
- `dd status=progress bs=1M if=/dev/sda of=/dev/sdb`

### hddtemp
- `watch -n 30 "sudo hddtemp /dev/sdg"`
- `sudo hddtemp /dev/sdg`

### blkid
- `sudo blkid`

### mount
- `sudo mount`



## SCSI Rödel
- https://forum.manjaro.org/t/solved-how-do-i-enable-sata-hotplug/2911/6
- `sudo echo 0 0 0 | sudo tee /sys/class/scsi_host/host*/scan`
